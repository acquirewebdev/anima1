$(document).ready(function() {
/*=====================================================
                        CONTACT US PAGE
=====================================================*/
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if( !emailReg.test( $email ) ) {
    return false;
    } else {
    return true; }
}
function alterCss(elem,msg,valid)  {
    if(!valid)
    {
        elem.css({
        'border' : '1px solid rgba(255, 0, 0, 0.62)',
        'background-color' : 'rgba(255,192,203)',
        'color' : '#C00',
        'outline':'none'
        })
        elem.attr("placeholder",msg).css({
            'color' : '#000'
        });
        elem.focus();
    }else{
        elem.css({
        'border' : '1px solid #CCC',
        'background-color' : '#FFF',
        'color' : '#000'
        })
    }
}

  $('#inputBtn').click(function() {
    var fname = $("#fname");
    var lname = $("#lname");
    var email = $("#email");
    var phone = $("#mobile");
    // var subject = $("#subject");
    var message = $("#message");

    var flag = false;

    if(fname.val() == "")
    {
        alterCss(fname,"Please input first name.");
        flag1 = false;
    }else{
        alterCss(fname,"Please input first name.",true);
        flag1 = true;
    }

    if(lname.val() == "")
    {
        alterCss(lname,"Please input last name.");
        flag2 = false;
    }else{
        alterCss(lname,"Please input last name.",true);
        flag2 = true;
    }

    if(email.val() == "")
    {
        alterCss(email,"Please input email.");
        flag3 = false;
    }else if(!validateEmail(email.val())){
        alterCss(email,"Please input valid email.");
        flag3 = false;
    }else{
        alterCss(email,"Please input valid email.",true);
        flag3 = true;
    }

    if(phone.val() == "")
    {
        alterCss(phone,"Please input phone.");
        flag4 = false;
     }else if(!$.isNumeric(phone.val()))
     {
         alterCss(phone,"Phone should be in digits.");
         flag4 = false;
    }
    else{
        alterCss(phone,"Phone should be in digits.",true);
        flag4 = true;
    }

    // if(subject.val() == "")
    // {
    //     alterCss(subject,"Please input Subject.");
    //     flag5 = false;
    // }else{
    //     alterCss(subject,"Please input Subject.",true);
    //     flag5 = true;
    // }

    
     if(message.val() == ""){
         alterCss(message,"Please input message.");
         flag5 = false;
     }else{
         alterCss(message,"Please input message.",true);
         flag5 = true;
     }

    flag = flag1 && flag2;
    flag = flag && flag3;
    flag = flag && flag4;
    flag = flag && flag5;
    // flag = flag && flag6;

    if(flag != false)
    {
        $("#contactusForm").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        $("#contactusForm").submit();
        return true;
    }
    return false;
    });
});