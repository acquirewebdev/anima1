$(document).ready(function() {
	
/*$("#theForm").validate({
  
    submitHandler: function(form) {
        $("#theForm").attr("action", "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
        form.submit();
    }
});*/

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if( !emailReg.test( $email ) ) {
    return false;
    } else {
    return true; }
}

function alterCss(elem,msg,valid)
{
    if(!valid)
    {
        elem.css({
        'border' : '1px solid rgba(255, 0, 0, 0.62)',
        'background-color' : 'pink',
        'color' : 'red'
        })
        elem.attr("placeholder",msg).css({
            'color' : '#000'
        });
        elem.focus();
    }else{
        elem.css({
        'border' : '1px solid #CCC',
        'background-color' : '#FFF',
        'color' : '#000'
        })
    }
    
    //elem.insertAfter("<span>" + msg + "</span>");
}

    $('.submit').click(function() {
        var fname = $("#first_name");
        var lname = $("#last_name");
        var email = $("#email");
        var phone = $("#mobile");
        //var company = $("#company");
        //var country = $("#countries");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
         }
        
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

        /*if(company.val() == "")
        {
            alterCss(company,"Please input company name.");
            flag5 = false;
        }else{
            alterCss(company,"Please input first name.",true);
            flag5 = true;
        }*/

        // if(country.val() == "")
        // {
        //     alterCss(country,"Please select country.");
        //     flag6 = false;
        // }else{
        //     alterCss(country,"Please input first name.",true);
        //     flag6 = true;
        // }

        // if(numbers.val() == "")
        // {
        //     alterCss(numbers,"Please input number of staff.");
        //     flag7 = false;
        // }else if(!$.isNumeric(numbers.val())){
        //     alterCss(numbers,"Number of staff should be in digits.");
        //     flag7 = false;
        // }else{
        //     alterCss(numbers,"Please input first name.",true);
        //     flag7 = true;
        // }

        // if(stafftype.val() == ""){
        //     alterCss(stafftype,"Please indicate staff type.");
        //     flag8 = false;
        // }else{
        //     alterCss(stafftype,"Please input first name.",true);
        //     flag8 = true;
        // }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        //flag = flag && flag5;
        // flag = flag && flag6;
        // flag = flag && flag7;
        // flag = flag && flag8;

        if(flag != false)
        {
            $("#theForm").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#theForm").submit();
            return true;
        }
        return false;
    });


/*==========================
contact us animation1
==========================*/
  $('.contactBtn').click(function() {
        var fname = $("#first_name");
        var lname = $("#last_name");
        var email = $("#email");
        var phone = $("#phone");
        var subject = $("#subject");
        var message = $("#description");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

        if(subject.val() == "")
        {
            alterCss(subject,"Please input Subject.");
            flag5 = false;
        }else{
            alterCss(subject,"Please input first name.",true);
            flag5 = true;
        }

        
         if(message.val() == ""){
             alterCss(message,"Please indicate staff type.");
             flag6 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false)
        {
            $("#theFormcontact").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#theFormcontact").submit();
            return true;
        }
        return false;
    });



/*==========================
contact us animation1
==========================*/
  $('.contactBtn0').click(function() {
        var fname = $("#first_name");
        var lname = $("#last_name");
        var email = $("#email");
        var phone = $("#mobile");
        var subject = $("#subject");
        var message = $("#description");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

        if(subject.val() == "")
        {
            alterCss(subject,"Please input Subject.");
            flag5 = false;
        }else{
            alterCss(subject,"Please input first name.",true);
            flag5 = true;
        }

        
         if(message.val() == ""){
             alterCss(message,"Please indicate staff type.");
             flag6 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false)
        {
            $("#theFormcontact").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#theFormcontact").submit();
            return true;
        }
        return false;
    });





/*==========================
Animation1 LP
==========================*/
$('.contactBtn2').click(function() {
        var fname = $("#first_name2");
        var lname = $("#last_name2");
        var email = $("#email2");
        var phone = $("#phone2");
        var subject = $("#subject2");
        var message = $("#description2");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

        if(subject.val() == "")
        {
            alterCss(subject,"Please input Subject.");
            flag5 = false;
        }else{
            alterCss(subject,"Please input first name.",true);
            flag5 = true;
        }

        
         if(message.val() == ""){
             alterCss(message,"Please indicate staff type.");
             flag6 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false)
        {
            $("#theFormcontact").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#theFormcontact").submit();
            return true;
        }
        return false;
    });


/*==========================
Animation1 LP
==========================*/
$('.contactBtn4').click(function() {
        var fname = $("#first_name4");
        var lname = $("#last_name4");
        var email = $("#email4");
        var phone = $("#phone4");
        var message = $("#description4");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;

        if(flag != false)
        {
            $("#servicelpc").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#servicelpc").submit();
            return true;
        }
        return false;
    });




/*==========================
GEO LP AU
==========================*/
$('.contactBtn_au').click(function() {
        var fname = $("#first_name_au");
        var lname = $("#last_name_au");
        var email = $("#email_au");
        var phone = $("#phone_au");
        var message = $("#description_au");
        var company = $("#company_au");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

          if(company.val() == ""){
             alterCss(company,"Please indicate company.");
             flag6 = false;
         }else{
             alterCss(company,"Please input company.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false) {
            $("#geo_lp_au").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#geo_lp_au").submit();
            return true;
        }
        return false;
    });



/*==========================
GEO LP UK
==========================*/
$('.contactBtn_uk').click(function() {
        var fname = $("#first_name_uk");
        var lname = $("#last_name_uk");
        var email = $("#email_uk");
        var phone = $("#phone_uk");
        var message = $("#description_uk");
        var company = $("#company_uk");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

          if(company.val() == ""){
             alterCss(company,"Please indicate company.");
             flag6 = false;
         }else{
             alterCss(company,"Please input company.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false)
        {
            $("#geo_lp_uk").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#geo_lp_uk").submit();
            return true;
        }
        return false;
    });




/*==========================
GEO LP AU
==========================*/
$('.contactBtn_us').click(function() {
        var fname = $("#first_name_us");
        var lname = $("#last_name_us");
        var email = $("#email_us");
        var phone = $("#phone_us");
        var message = $("#description_us");
        var company = $("#company_us");
        //var numbers = $("#numbers");
        //var stafftype = $("#staff");
        //var captcha   = $("#recaptcha_response_field");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }

        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

          if(company.val() == ""){
             alterCss(company,"Please indicate company.");
             flag6 = false;
         }else{
             alterCss(company,"Please input company.",true);
             flag6 = true;
         }

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
        flag = flag && flag6;

        if(flag != false)
        {
            $("#geo_lp_us").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#geo_lp_us").submit();
            return true;
        }
        return false;
    });

/*===================
    MOBILE APP AU
===================*/
$('.mobileapp_btn').click(function() {
        var fname = $("#fname");
        var lname = $("#lname");
        var email = $("#email");
        var phone = $("#mobile");
        var message = $("#message");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }
        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

  

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
      

        if(flag != false)
        {
            $("#mobileapp_au").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#mobileapp_au").submit();
            return true;
        }
        return false;
    });

/*===================
    AUGMENTED LP
===================*/
$('.augmented_btn').click(function() {
        var fname = $("#fname");
        var lname = $("#lname");
        var email = $("#email");
        var phone = $("#mobile");
        var message = $("#message");

        var flag = false;

        if(fname.val() == "")
        {
            alterCss(fname,"Please input first name.");
            flag1 = false;
        }else{
            alterCss(fname,"Please input first name.",true);
            flag1 = true;
        }
        if(lname.val() == "")
        {
            alterCss(lname,"Please input last name.");
            flag2 = false;
        }else{
            alterCss(lname,"Please input first name.",true);
            flag2 = true;
        }

        if(email.val() == "")
        {
            alterCss(email,"Please input email.");
            flag3 = false;
        }else if(!validateEmail(email.val())){
            alterCss(email,"Please input valid email.");
            flag3 = false;
        }else{
            alterCss(email,"Please input first name.",true);
            flag3 = true;
        }

        if(phone.val() == "")
        {
            alterCss(phone,"Please input phone.");
            flag4 = false;
         }else if(!$.isNumeric(phone.val()))
         {
             alterCss(phone,"Phone should be in digits.");
             flag4 = false;
        }
        else{
            alterCss(phone,"Please input first name.",true);
            flag4 = true;
        }

      
         if(message.val() == ""){
             alterCss(message,"Please indicate Message.");
             flag5 = false;
         }else{
             alterCss(message,"Please input first name.",true);
             flag5 = true;
         }

  

        flag = flag1 && flag2;
        flag = flag && flag3;
        flag = flag && flag4;
        flag = flag && flag5;
      

        if(flag != false)
        {
            $("#augmented_lp").attr("action","https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8");
            $("#augmented_lp").submit();
            return true;
        }
        return false;
    });


    $(".sis").on("change keydown keypress keyup",function(){
        var subject = "Subject:" + $("#subject").val();
        $("#message").val(subject + "\n");
    });
});