<?php
/*
Plugin Name: Sales Force Plugin
Plugin URI: http://animation1.com
Description: A plugin for Sales Force Web to Lead Form
Version: 1.0
Author: Ronnie Nucum & Alvin Sorreda
Author URI: http://creativelab.ph
*/
function getAreacode($ccode) {
    $countries = array();
    $countries[] = array("code"=>"AF","name"=>"Afghanistan","d_code"=>"+93");
    $countries[] = array("code"=>"AL","name"=>"Albania","d_code"=>"+355");
    $countries[] = array("code"=>"DZ","name"=>"Algeria","d_code"=>"+213");
    $countries[] = array("code"=>"AS","name"=>"American Samoa","d_code"=>"+1");
    $countries[] = array("code"=>"AD","name"=>"Andorra","d_code"=>"+376");
    $countries[] = array("code"=>"AO","name"=>"Angola","d_code"=>"+244");
    $countries[] = array("code"=>"AI","name"=>"Anguilla","d_code"=>"+1");
    $countries[] = array("code"=>"AG","name"=>"Antigua","d_code"=>"+1");
    $countries[] = array("code"=>"AR","name"=>"Argentina","d_code"=>"+54");
    $countries[] = array("code"=>"AM","name"=>"Armenia","d_code"=>"+374");
    $countries[] = array("code"=>"AW","name"=>"Aruba","d_code"=>"+297");
    $countries[] = array("code"=>"AU","name"=>"Australia","d_code"=>"+61");
    $countries[] = array("code"=>"AT","name"=>"Austria","d_code"=>"+43");
    $countries[] = array("code"=>"AZ","name"=>"Azerbaijan","d_code"=>"+994");
    $countries[] = array("code"=>"BH","name"=>"Bahrain","d_code"=>"+973");
    $countries[] = array("code"=>"BD","name"=>"Bangladesh","d_code"=>"+880");
    $countries[] = array("code"=>"BB","name"=>"Barbados","d_code"=>"+1");
    $countries[] = array("code"=>"BY","name"=>"Belarus","d_code"=>"+375");
    $countries[] = array("code"=>"BE","name"=>"Belgium","d_code"=>"+32");
    $countries[] = array("code"=>"BZ","name"=>"Belize","d_code"=>"+501");
    $countries[] = array("code"=>"BJ","name"=>"Benin","d_code"=>"+229");
    $countries[] = array("code"=>"BM","name"=>"Bermuda","d_code"=>"+1");
    $countries[] = array("code"=>"BT","name"=>"Bhutan","d_code"=>"+975");
    $countries[] = array("code"=>"BO","name"=>"Bolivia","d_code"=>"+591");
    $countries[] = array("code"=>"BA","name"=>"Bosnia and Herzegovina","d_code"=>"+387");
    $countries[] = array("code"=>"BW","name"=>"Botswana","d_code"=>"+267");
    $countries[] = array("code"=>"BR","name"=>"Brazil","d_code"=>"+55");
    $countries[] = array("code"=>"IO","name"=>"British Indian Ocean Territory","d_code"=>"+246");
    $countries[] = array("code"=>"VG","name"=>"British Virgin Islands","d_code"=>"+1");
    $countries[] = array("code"=>"BN","name"=>"Brunei","d_code"=>"+673");
    $countries[] = array("code"=>"BG","name"=>"Bulgaria","d_code"=>"+359");
    $countries[] = array("code"=>"BF","name"=>"Burkina Faso","d_code"=>"+226");
    $countries[] = array("code"=>"MM","name"=>"Burma Myanmar" ,"d_code"=>"+95");
    $countries[] = array("code"=>"BI","name"=>"Burundi","d_code"=>"+257");
    $countries[] = array("code"=>"KH","name"=>"Cambodia","d_code"=>"+855");
    $countries[] = array("code"=>"CM","name"=>"Cameroon","d_code"=>"+237");
    $countries[] = array("code"=>"CA","name"=>"Canada","d_code"=>"+1");
    $countries[] = array("code"=>"CV","name"=>"Cape Verde","d_code"=>"+238");
    $countries[] = array("code"=>"KY","name"=>"Cayman Islands","d_code"=>"+1");
    $countries[] = array("code"=>"CF","name"=>"Central African Republic","d_code"=>"+236");
    $countries[] = array("code"=>"TD","name"=>"Chad","d_code"=>"+235");
    $countries[] = array("code"=>"CL","name"=>"Chile","d_code"=>"+56");
    $countries[] = array("code"=>"CN","name"=>"China","d_code"=>"+86");
    $countries[] = array("code"=>"CO","name"=>"Colombia","d_code"=>"+57");
    $countries[] = array("code"=>"KM","name"=>"Comoros","d_code"=>"+269");
    $countries[] = array("code"=>"CK","name"=>"Cook Islands","d_code"=>"+682");
    $countries[] = array("code"=>"CR","name"=>"Costa Rica","d_code"=>"+506");
    $countries[] = array("code"=>"CI","name"=>"Côte d'Ivoire" ,"d_code"=>"+225");
    $countries[] = array("code"=>"HR","name"=>"Croatia","d_code"=>"+385");
    $countries[] = array("code"=>"CU","name"=>"Cuba","d_code"=>"+53");
    $countries[] = array("code"=>"CY","name"=>"Cyprus","d_code"=>"+357");
    $countries[] = array("code"=>"CZ","name"=>"Czech Republic","d_code"=>"+420");
    $countries[] = array("code"=>"CD","name"=>"Democratic Republic of Congo","d_code"=>"+243");
    $countries[] = array("code"=>"DK","name"=>"Denmark","d_code"=>"+45");
    $countries[] = array("code"=>"DJ","name"=>"Djibouti","d_code"=>"+253");
    $countries[] = array("code"=>"DM","name"=>"Dominica","d_code"=>"+1");
    $countries[] = array("code"=>"DO","name"=>"Dominican Republic","d_code"=>"+1");
    $countries[] = array("code"=>"EC","name"=>"Ecuador","d_code"=>"+593");
    $countries[] = array("code"=>"EG","name"=>"Egypt","d_code"=>"+20");
    $countries[] = array("code"=>"SV","name"=>"El Salvador","d_code"=>"+503");
    $countries[] = array("code"=>"GQ","name"=>"Equatorial Guinea","d_code"=>"+240");
    $countries[] = array("code"=>"ER","name"=>"Eritrea","d_code"=>"+291");
    $countries[] = array("code"=>"EE","name"=>"Estonia","d_code"=>"+372");
    $countries[] = array("code"=>"ET","name"=>"Ethiopia","d_code"=>"+251");
    $countries[] = array("code"=>"FK","name"=>"Falkland Islands","d_code"=>"+500");
    $countries[] = array("code"=>"FO","name"=>"Faroe Islands","d_code"=>"+298");
    $countries[] = array("code"=>"FM","name"=>"Federated States of Micronesia","d_code"=>"+691");
    $countries[] = array("code"=>"FJ","name"=>"Fiji","d_code"=>"+679");
    $countries[] = array("code"=>"FI","name"=>"Finland","d_code"=>"+358");
    $countries[] = array("code"=>"FR","name"=>"France","d_code"=>"+33");
    $countries[] = array("code"=>"GF","name"=>"French Guiana","d_code"=>"+594");
    $countries[] = array("code"=>"PF","name"=>"French Polynesia","d_code"=>"+689");
    $countries[] = array("code"=>"GA","name"=>"Gabon","d_code"=>"+241");
    $countries[] = array("code"=>"GE","name"=>"Georgia","d_code"=>"+995");
    $countries[] = array("code"=>"DE","name"=>"Germany","d_code"=>"+49");
    $countries[] = array("code"=>"GH","name"=>"Ghana","d_code"=>"+233");
    $countries[] = array("code"=>"GI","name"=>"Gibraltar","d_code"=>"+350");
    $countries[] = array("code"=>"GR","name"=>"Greece","d_code"=>"+30");
    $countries[] = array("code"=>"GL","name"=>"Greenland","d_code"=>"+299");
    $countries[] = array("code"=>"GD","name"=>"Grenada","d_code"=>"+1");
    $countries[] = array("code"=>"GP","name"=>"Guadeloupe","d_code"=>"+590");
    $countries[] = array("code"=>"GU","name"=>"Guam","d_code"=>"+1");
    $countries[] = array("code"=>"GT","name"=>"Guatemala","d_code"=>"+502");
    $countries[] = array("code"=>"GN","name"=>"Guinea","d_code"=>"+224");
    $countries[] = array("code"=>"GW","name"=>"Guinea-Bissau","d_code"=>"+245");
    $countries[] = array("code"=>"GY","name"=>"Guyana","d_code"=>"+592");
    $countries[] = array("code"=>"HT","name"=>"Haiti","d_code"=>"+509");
    $countries[] = array("code"=>"HN","name"=>"Honduras","d_code"=>"+504");
    $countries[] = array("code"=>"HK","name"=>"Hong Kong","d_code"=>"+852");
    $countries[] = array("code"=>"HU","name"=>"Hungary","d_code"=>"+36");
    $countries[] = array("code"=>"IS","name"=>"Iceland","d_code"=>"+354");
    $countries[] = array("code"=>"IN","name"=>"India","d_code"=>"+91");
    $countries[] = array("code"=>"ID","name"=>"Indonesia","d_code"=>"+62");
    $countries[] = array("code"=>"IR","name"=>"Iran","d_code"=>"+98");
    $countries[] = array("code"=>"IQ","name"=>"Iraq","d_code"=>"+964");
    $countries[] = array("code"=>"IE","name"=>"Ireland","d_code"=>"+353");
    $countries[] = array("code"=>"IL","name"=>"Israel","d_code"=>"+972");
    $countries[] = array("code"=>"IT","name"=>"Italy","d_code"=>"+39");
    $countries[] = array("code"=>"JM","name"=>"Jamaica","d_code"=>"+1");
    $countries[] = array("code"=>"JP","name"=>"Japan","d_code"=>"+81");
    $countries[] = array("code"=>"JO","name"=>"Jordan","d_code"=>"+962");
    $countries[] = array("code"=>"KZ","name"=>"Kazakhstan","d_code"=>"+7");
    $countries[] = array("code"=>"KE","name"=>"Kenya","d_code"=>"+254");
    $countries[] = array("code"=>"KI","name"=>"Kiribati","d_code"=>"+686");
    $countries[] = array("code"=>"XK","name"=>"Kosovo","d_code"=>"+381");
    $countries[] = array("code"=>"KW","name"=>"Kuwait","d_code"=>"+965");
    $countries[] = array("code"=>"KG","name"=>"Kyrgyzstan","d_code"=>"+996");
    $countries[] = array("code"=>"LA","name"=>"Laos","d_code"=>"+856");
    $countries[] = array("code"=>"LV","name"=>"Latvia","d_code"=>"+371");
    $countries[] = array("code"=>"LB","name"=>"Lebanon","d_code"=>"+961");
    $countries[] = array("code"=>"LS","name"=>"Lesotho","d_code"=>"+266");
    $countries[] = array("code"=>"LR","name"=>"Liberia","d_code"=>"+231");
    $countries[] = array("code"=>"LY","name"=>"Libya","d_code"=>"+218");
    $countries[] = array("code"=>"LI","name"=>"Liechtenstein","d_code"=>"+423");
    $countries[] = array("code"=>"LT","name"=>"Lithuania","d_code"=>"+370");
    $countries[] = array("code"=>"LU","name"=>"Luxembourg","d_code"=>"+352");
    $countries[] = array("code"=>"MO","name"=>"Macau","d_code"=>"+853");
    $countries[] = array("code"=>"MK","name"=>"Macedonia","d_code"=>"+389");
    $countries[] = array("code"=>"MG","name"=>"Madagascar","d_code"=>"+261");
    $countries[] = array("code"=>"MW","name"=>"Malawi","d_code"=>"+265");
    $countries[] = array("code"=>"MY","name"=>"Malaysia","d_code"=>"+60");
    $countries[] = array("code"=>"MV","name"=>"Maldives","d_code"=>"+960");
    $countries[] = array("code"=>"ML","name"=>"Mali","d_code"=>"+223");
    $countries[] = array("code"=>"MT","name"=>"Malta","d_code"=>"+356");
    $countries[] = array("code"=>"MH","name"=>"Marshall Islands","d_code"=>"+692");
    $countries[] = array("code"=>"MQ","name"=>"Martinique","d_code"=>"+596");
    $countries[] = array("code"=>"MR","name"=>"Mauritania","d_code"=>"+222");
    $countries[] = array("code"=>"MU","name"=>"Mauritius","d_code"=>"+230");
    $countries[] = array("code"=>"YT","name"=>"Mayotte","d_code"=>"+262");
    $countries[] = array("code"=>"MX","name"=>"Mexico","d_code"=>"+52");
    $countries[] = array("code"=>"MD","name"=>"Moldova","d_code"=>"+373");
    $countries[] = array("code"=>"MC","name"=>"Monaco","d_code"=>"+377");
    $countries[] = array("code"=>"MN","name"=>"Mongolia","d_code"=>"+976");
    $countries[] = array("code"=>"ME","name"=>"Montenegro","d_code"=>"+382");
    $countries[] = array("code"=>"MS","name"=>"Montserrat","d_code"=>"+1");
    $countries[] = array("code"=>"MA","name"=>"Morocco","d_code"=>"+212");
    $countries[] = array("code"=>"MZ","name"=>"Mozambique","d_code"=>"+258");
    $countries[] = array("code"=>"NA","name"=>"Namibia","d_code"=>"+264");
    $countries[] = array("code"=>"NR","name"=>"Nauru","d_code"=>"+674");
    $countries[] = array("code"=>"NP","name"=>"Nepal","d_code"=>"+977");
    $countries[] = array("code"=>"NL","name"=>"Netherlands","d_code"=>"+31");
    $countries[] = array("code"=>"AN","name"=>"Netherlands Antilles","d_code"=>"+599");
    $countries[] = array("code"=>"NC","name"=>"New Caledonia","d_code"=>"+687");
    $countries[] = array("code"=>"NZ","name"=>"New Zealand","d_code"=>"+64");
    $countries[] = array("code"=>"NI","name"=>"Nicaragua","d_code"=>"+505");
    $countries[] = array("code"=>"NE","name"=>"Niger","d_code"=>"+227");
    $countries[] = array("code"=>"NG","name"=>"Nigeria","d_code"=>"+234");
    $countries[] = array("code"=>"NU","name"=>"Niue","d_code"=>"+683");
    $countries[] = array("code"=>"NF","name"=>"Norfolk Island","d_code"=>"+672");
    $countries[] = array("code"=>"KP","name"=>"North Korea","d_code"=>"+850");
    $countries[] = array("code"=>"MP","name"=>"Northern Mariana Islands","d_code"=>"+1");
    $countries[] = array("code"=>"NO","name"=>"Norway","d_code"=>"+47");
    $countries[] = array("code"=>"OM","name"=>"Oman","d_code"=>"+968");
    $countries[] = array("code"=>"PK","name"=>"Pakistan","d_code"=>"+92");
    $countries[] = array("code"=>"PW","name"=>"Palau","d_code"=>"+680");
    $countries[] = array("code"=>"PS","name"=>"Palestine","d_code"=>"+970");
    $countries[] = array("code"=>"PA","name"=>"Panama","d_code"=>"+507");
    $countries[] = array("code"=>"PG","name"=>"Papua New Guinea","d_code"=>"+675");
    $countries[] = array("code"=>"PY","name"=>"Paraguay","d_code"=>"+595");
    $countries[] = array("code"=>"PE","name"=>"Peru","d_code"=>"+51");
    $countries[] = array("code"=>"PH","name"=>"Philippines","d_code"=>"+63");
    $countries[] = array("code"=>"PL","name"=>"Poland","d_code"=>"+48");
    $countries[] = array("code"=>"PT","name"=>"Portugal","d_code"=>"+351");
    $countries[] = array("code"=>"PR","name"=>"Puerto Rico","d_code"=>"+1");
    $countries[] = array("code"=>"QA","name"=>"Qatar","d_code"=>"+974");
    $countries[] = array("code"=>"CG","name"=>"Republic of the Congo","d_code"=>"+242");
    $countries[] = array("code"=>"RE","name"=>"Réunion" ,"d_code"=>"+262");
    $countries[] = array("code"=>"RO","name"=>"Romania","d_code"=>"+40");
    $countries[] = array("code"=>"RU","name"=>"Russia","d_code"=>"+7");
    $countries[] = array("code"=>"RW","name"=>"Rwanda","d_code"=>"+250");
    $countries[] = array("code"=>"BL","name"=>"Saint Barthélemy" ,"d_code"=>"+590");
    $countries[] = array("code"=>"SH","name"=>"Saint Helena","d_code"=>"+290");
    $countries[] = array("code"=>"KN","name"=>"Saint Kitts and Nevis","d_code"=>"+1");
    $countries[] = array("code"=>"MF","name"=>"Saint Martin","d_code"=>"+590");
    $countries[] = array("code"=>"PM","name"=>"Saint Pierre and Miquelon","d_code"=>"+508");
    $countries[] = array("code"=>"VC","name"=>"Saint Vincent and the Grenadines","d_code"=>"+1");
    $countries[] = array("code"=>"WS","name"=>"Samoa","d_code"=>"+685");
    $countries[] = array("code"=>"SM","name"=>"San Marino","d_code"=>"+378");
    $countries[] = array("code"=>"ST","name"=>"São Tomé and Príncipe" ,"d_code"=>"+239");
    $countries[] = array("code"=>"SA","name"=>"Saudi Arabia","d_code"=>"+966");
    $countries[] = array("code"=>"SN","name"=>"Senegal","d_code"=>"+221");
    $countries[] = array("code"=>"RS","name"=>"Serbia","d_code"=>"+381");
    $countries[] = array("code"=>"SC","name"=>"Seychelles","d_code"=>"+248");
    $countries[] = array("code"=>"SL","name"=>"Sierra Leone","d_code"=>"+232");
    $countries[] = array("code"=>"SG","name"=>"Singapore","d_code"=>"+65");
    $countries[] = array("code"=>"SK","name"=>"Slovakia","d_code"=>"+421");
    $countries[] = array("code"=>"SI","name"=>"Slovenia","d_code"=>"+386");
    $countries[] = array("code"=>"SB","name"=>"Solomon Islands","d_code"=>"+677");
    $countries[] = array("code"=>"SO","name"=>"Somalia","d_code"=>"+252");
    $countries[] = array("code"=>"ZA","name"=>"South Africa","d_code"=>"+27");
    $countries[] = array("code"=>"KR","name"=>"South Korea","d_code"=>"+82");
    $countries[] = array("code"=>"ES","name"=>"Spain","d_code"=>"+34");
    $countries[] = array("code"=>"LK","name"=>"Sri Lanka","d_code"=>"+94");
    $countries[] = array("code"=>"LC","name"=>"St. Lucia","d_code"=>"+1");
    $countries[] = array("code"=>"SD","name"=>"Sudan","d_code"=>"+249");
    $countries[] = array("code"=>"SR","name"=>"Suriname","d_code"=>"+597");
    $countries[] = array("code"=>"SZ","name"=>"Swaziland","d_code"=>"+268");
    $countries[] = array("code"=>"SE","name"=>"Sweden","d_code"=>"+46");
    $countries[] = array("code"=>"CH","name"=>"Switzerland","d_code"=>"+41");
    $countries[] = array("code"=>"SY","name"=>"Syria","d_code"=>"+963");
    $countries[] = array("code"=>"TW","name"=>"Taiwan","d_code"=>"+886");
    $countries[] = array("code"=>"TJ","name"=>"Tajikistan","d_code"=>"+992");
    $countries[] = array("code"=>"TZ","name"=>"Tanzania","d_code"=>"+255");
    $countries[] = array("code"=>"TH","name"=>"Thailand","d_code"=>"+66");
    $countries[] = array("code"=>"BS","name"=>"The Bahamas","d_code"=>"+1");
    $countries[] = array("code"=>"GM","name"=>"The Gambia","d_code"=>"+220");
    $countries[] = array("code"=>"TL","name"=>"Timor-Leste","d_code"=>"+670");
    $countries[] = array("code"=>"TG","name"=>"Togo","d_code"=>"+228");
    $countries[] = array("code"=>"TK","name"=>"Tokelau","d_code"=>"+690");
    $countries[] = array("code"=>"TO","name"=>"Tonga","d_code"=>"+676");
    $countries[] = array("code"=>"TT","name"=>"Trinidad and Tobago","d_code"=>"+1");
    $countries[] = array("code"=>"TN","name"=>"Tunisia","d_code"=>"+216");
    $countries[] = array("code"=>"TR","name"=>"Turkey","d_code"=>"+90");
    $countries[] = array("code"=>"TM","name"=>"Turkmenistan","d_code"=>"+993");
    $countries[] = array("code"=>"TC","name"=>"Turks and Caicos Islands","d_code"=>"+1");
    $countries[] = array("code"=>"TV","name"=>"Tuvalu","d_code"=>"+688");
    $countries[] = array("code"=>"UG","name"=>"Uganda","d_code"=>"+256");
    $countries[] = array("code"=>"UA","name"=>"Ukraine","d_code"=>"+380");
    $countries[] = array("code"=>"AE","name"=>"United Arab Emirates","d_code"=>"+971");
    $countries[] = array("code"=>"GB","name"=>"United Kingdom","d_code"=>"+44");
    $countries[] = array("code"=>"US","name"=>"United States","d_code"=>"+1");
    $countries[] = array("code"=>"UY","name"=>"Uruguay","d_code"=>"+598");
    $countries[] = array("code"=>"VI","name"=>"US Virgin Islands","d_code"=>"+1");
    $countries[] = array("code"=>"UZ","name"=>"Uzbekistan","d_code"=>"+998");
    $countries[] = array("code"=>"VU","name"=>"Vanuatu","d_code"=>"+678");
    $countries[] = array("code"=>"VA","name"=>"Vatican City","d_code"=>"+39");
    $countries[] = array("code"=>"VE","name"=>"Venezuela","d_code"=>"+58");
    $countries[] = array("code"=>"VN","name"=>"Vietnam","d_code"=>"+84");
    $countries[] = array("code"=>"WF","name"=>"Wallis and Futuna","d_code"=>"+681");
    $countries[] = array("code"=>"YE","name"=>"Yemen","d_code"=>"+967");
    $countries[] = array("code"=>"ZM","name"=>"Zambia","d_code"=>"+260");
    $countries[] = array("code"=>"ZW","name"=>"Zimbabwe","d_code"=>"+263");
    foreach($countries as $value) {
        if($value['code'] == $ccode) {
            return $value['d_code'];
        }     
    }
}

function displayDDLeadSource($leadsource){
    $leadsource =  htmlspecialchars(trim($leadsource));
    $leadsource = str_replace("\\", "", $leadsource);
    $leadsources = array(
        'Adroll',
        'Advertisement',
        'Adwords',
        'Bing Ads',
        'Call',
        'Cohort',
        'Consultant',
        'Company Website',
        'Customer Referral',
        'External Referral',
        'Internal Referral',
        "Mike's Tours",
        'Networking Event',
        'Other',
        'Partner',
        'Public Relations',
        'Seminar - Internal',
        'Seminar - Partner',
        'Telemarketing',
        'Trade Show / Conference',
        'Web',
        'Word Of Mouth'
    );

    if( $leadsource == "" || !(in_array(ucwords($leadsource), $leadsources)) )
    {
        return leadsourceTemplate("Company Website");
    }

    if(in_array(ucwords($leadsource), $leadsources))
    {
        return leadsourceTemplate(ucwords($leadsource));
    }
}

function leadsourceTemplate($leadsource)
{
    return <<<EOL
    <label for="lead_source" style="display: none;">Lead Source</label>
    <select  id="lead_source" name="lead_source" style="display: none;">
        <option value="$leadsource">$leadsource</option>
    </select>
EOL;
}


function getReferer()
{
    if(isset($_SESSION['refer']))
    {
        return $_SESSION['refer'];
    }
    elseif(isset($_SERVER['HTTP_REFERER'])) {
        return $_SERVER['HTTP_REFERER'];
    }
    return "Direct Referral";
}


function fetchLocation() {
    $ip = $_SERVER['REMOTE_ADDR'];  
    //$ip = "127.0.0.1"; //$_SERVER['REMOTE_ADDR'];
    $query = file_get_contents('http://ip-api.com/json/'.$ip);
    $info = json_decode($query);
    if(!empty($info)) {
        return $info;
    }
}

function GetIP() {
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
    {
        if (array_key_exists($key, $_SERVER) === true)
        {
            foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip)
            {
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
                {
                    return $ip;
                }
            }
        }
    }
}

function contact_form() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>

TA;
return <<<EOL
<form id="theFormcontact" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="701900000011Wr2" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="Animation1 - Website">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="http://animation1.com" />
        <div>
            <input id="first_name" maxlength="40" name="first_name" size="20" type="text"  value="First Name:" />
            <input  id="last_name" maxlength="80" name="last_name" size="20" type="text"   value="Last Name:" />
        </div>
        <input  id="email" maxlength="80" name="email" size="20" type="text" class="textfield" value="Email"/>
        <input  id="mobile" maxlength="40" name="mobile" size="20" type="text" class="textfield" value="Phone:"/>
        <input  id="subject" maxlength="40" name="subject" size="20" type="text" class="textfield sis" value="Subject:"/>
        <textarea class="textfield" name="description">Message:</textarea>
        $trackinfo
        <input type="submit" name="submit" class="contactBtn0" value="Send">
</form>
EOL;
}
add_shortcode('sf_form_contact','contact_form');

function adwords_lead_form() {
 $getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$keyword = fetchKeyword(get_query_var('keyword'));    
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;
/*$cidArray = array('698' => '701900000011bJu');
$pid = get_the_ID();
if(array_key_exists($pid,$cidArray)) {
    if(is_single($pid)) {
        $cid = $cidArray[$pid];
    }else{
        $cid = "701900000011WM0";
    }
} else {
    $cid = "701900000011WM0";
}*/
return <<<EOL
<form id="theForm" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="701900000011bJu" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="Animation1 - Adwords">
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<table width="100%">
<tbody>
<tr>
    <td style="vertical-align: top !important;" valign="top">First Name</td>
    <td style="vertical-align: top !important;" valign="top">:</td>
    <td style="vertical-align: top !important;" valign="top">
        <input id="first_name" maxlength="40" name="first_name" size="20" type="text" class="required" />
    </td>
</tr>
<tr>
    <td style="vertical-align: top !important;" valign="top">Last Name</td>
    <td style="vertical-align: top !important;" valign="top">:</td>
    <td style="vertical-align: top !important;" valign="top">
        <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" />
    </td>
</tr>
<tr>
    <td style="vertical-align: top !important;" valign="top">Email</td>
    <td style="vertical-align: top !important;" valign="top">&nbsp;:&nbsp;</td>
    <td style="vertical-align: top !important;" valign="top">
        <input  id="email" maxlength="80" name="email" size="20" type="text" />
    </td>
</tr>
<tr>
    <td style="vertical-align: top !important;" valign="top">Phone</td>
    <td style="vertical-align: top !important;" valign="top">&nbsp;:&nbsp;</td>
    <td style="vertical-align: top !important;" valign="top">
        <input  id="mobile" maxlength="40" name="mobile" size="20" type="text" />
    </td>
</tr>
<tr>
    <td style="vertical-align: top !important;" valign="top">Message</td>
    <td style="vertical-align: top !important;" valign="top">&nbsp;:&nbsp;</td>
    <td style="vertical-align: top !important;" valign="top">
        <textarea name="description"></textarea>
    </td>
</tr>
<tr>
    <td>
        $trackinfo
    </td>
</tr>
<tr>
<td></td>
<td></td>
<td><input type="submit" name="submit" class="submit"></td>
</tr>
</tbody>
</table>
</form>
EOL;
}
add_shortcode('sf_form','adwords_lead_form');

function treatment_lp() {
   $getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$keyword = fetchKeyword(get_query_var('keyword'));
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

 $dataSet = array();
 $cidArray = array(
    '839' =>  array('cid' => '701900000011hGI', 'title' => 'Animation1 - Adwords - Treatment - A', 'url' => 'http://animation1.com/3dservice-lpa/'), 
    '840' =>  array('cid' => '701900000011hGD', 'title' => 'Animation1 - Adwords - Treatment - B', 'url' => 'http://animation1.com/3dservice-lpb/'),    
    '839' =>  array('cid' => '701900000011kbF', 'title' => '', 'url' => 'http://animation1.com/3dservice-lpc/'), 
    '843' =>  array('cid' => '701900000011hGD', 'title' => 'FB-PPC', 'url' => 'http://animation1.com/fb-3dservice-lpb/'));
 
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray))
 {
    if(is_page($pid))
   {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
        //$cid = $cidArray[$pid];
     }else{
        $cid = "";
     }
 }else{
     $cid = "";
 }

// $pidArray = array(
//    '839' => array('cid' => '701900000011hGI', 
//                   'websource' => 'http://animation1.com/3dservice-lpa/'),
//    '849' => array('cid' => '701900000011hGD',
//                   'websource' => 'http://animation1.com/3dservice-lpb/'));
// $pid = get_the_ID();
// if(array_key_exist($pid,$pidArray)) {
//     if(is_page($pid)) {
//         $cid[] = $pidArray[$pid];
//     } else {
//         $cid = "";
//     }
//     else {
//         $cid = "";
//     }
// }
return <<<EOL
<form id="cc_lp_form" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="first_name" maxlength="40" name="first_name" size="20" type="text"  value="First Name:" />
  <input  id="last_name" maxlength="80" name="last_name" size="20" type="text"   value="Last Name:" />
</div>
<div>
<input  id="email" maxlength="80" name="email" size="20" type="text" class="textfield" value="Email:"/>  
<input  id="mobile" maxlength="40" name="mobile" size="20" type="text" class="textfield" value="Phone:"/>
<input  id="company" maxlength="40" name="company" size="20" type="text" class="textfield" value="Company:"/>
<textarea class="textfield" id="description" name="description">Message:</textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn" value="Contact Us Today">
</form>

EOL;
}
add_shortcode('sf_form_treatment','treatment_lp');


/*==========================
LP FORM PUTE
===========================*/
function service_lp_c() {
    $getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$keyword = fetchKeyword(get_query_var('keyword'));
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

 $dataSet = array();
 $cidArray = array(
    '850' =>  array('cid' => '701900000012MZc',  'url' => 'http://animation1.com/2d-lpc-1/'), 
    '847' =>  array('cid' => '701900000012MZm',  'url' => 'http://animation1.com/3d-lpc-1/'),    
    '848' =>  array('cid' => '701900000012MZw',  'url' => 'http://animation1.com/ar-lpc_1/'), 
    '849' =>  array('cid' => '701900000012MaB',  'url' => 'http://animation1.com/vis-lpc_1/'), 
    '851' =>  array('cid' => '701900000012Ma1',  'url' => 'http://animation1.com/arch-lpc-1/'));
 
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray))
 {
    if(is_page($pid))
   {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['url'];
        }
    }
        //$cid = $cidArray[$pid];
     }else{
        $cid = "";
     }
 }else{
     $cid = "";
 }

return <<<EOL
<form id="servicelpc" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data2" />
<div>
  <input id="first_name4" maxlength="40" name="first_name" size="20" type="text" placeholder="First Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input  id="last_name4" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email4" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="phone4" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<input  id="company4" maxlength="40" name="company" size="20" type="text" class="textfield" placeholder="Company:"/>
<textarea class="textfield" id="description4" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn4" value="Contact Us Today">
</form>
EOL;
}
add_shortcode('sf_form_c','service_lp_c');


/*==========================
LP GEO AU
===========================*/
function geo_lpa_au() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer(); 
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource')); 
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

 $dataSet = array();
 $cidArray = array(
    '856' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalrendering-lpa/'),
    '890' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalrendering-lpb/'),
    '857' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalanimation-lpa/'),
    '891' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalanimation-lpb/'),
    '859' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalvisualization-lpa/'),
    '892' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-architecturalvisualization-lpb/'),
    '870' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3danimation-lpa/'),
    '925' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3danimation-lpb/'),
    '871' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3drendering-lpa/'),
    '894' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3drendering-lpb/'),
    '874' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3dvisualization-lpa/'),
    '895' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3dvisualization-lpb/'),
    '878' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3dmodeling-lpa/'),
    '924' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-3dmodeling-lpb/'),
    '883' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2danimation-lpa/'),
    '896' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2danimation-lpb/'),
    '886' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2drendering-lpa/'),
    '897' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2drendering-lpb/'),
    '888' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2dvisualization-lpa/'),
    '898' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2dvisualization-lpb/'),
    '889' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2dmodeling-lpa/'),
    '899' =>  array('cid' => '701900000012U6G',  'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/au-2dmodeling-lpb/'));
 
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray))
 {
    if(is_page($pid))
   {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
        //$cid = $cidArray[$pid];
     }else{
        $cid = "";
     }
 }else{
     $cid = "";
 }
return <<<EOL
<form id="geo_lp_au" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="first_name_au" maxlength="40" name="first_name" size="20" type="text" placeholder="First Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input  id="last_name_au" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email_au" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="phone_au" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<input  id="company_au" maxlength="40" name="company" size="20" type="text" class="textfield" placeholder="Company:"/>
<textarea class="textfield" id="description_au" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn_au" value="Contact Us Today">
</form>
EOL;
}
add_shortcode('sf_geo_au','geo_lpa_au');

/*==========================
LP GEO UK
===========================*/
function geo_lpa_uk() {
   $getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$keyword = fetchKeyword(get_query_var('keyword')); 
$leadsource = displayDDLeadSource(get_query_var('leadsource'));   
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

 $dataSet = array();
 $cidArray = array(
    '860' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalrendering-lpa/'), 
    '861' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalanimation-lpa/'),    
    '862' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalvisualization-lpa/'), 
    '863' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3danimation-lpa/'), 
    '864' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3drendering-lpa/'),
    '865' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3dvisualization-lpa/'),
    '866' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3dmodeling-lpa/'),
    '867' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2danimation-lpa'),
    '868' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2drendering-lpa/'),
    '869' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2dvisualization-lpa/'),
    '872' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2dmodeling-lpa/'),
    '873' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalrendering-lpb/'),
    '875' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalanimation-lpb/'),
    '876' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-architecturalvisualization-lpb/'),
    '877' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3danimation-lpb/'),
    '879' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3drendering-lpb/'),
    '880' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3dvisualization-lpb/'),
    '881' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-3dmodeling-lpb/'),
    '882' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2danimation-lpb/'),
    '884' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2drendering-lpb/'),
    '885' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2dvisualization-lpb/'),
    '887' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-2dmodeling-lpb/'));
 
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray))
 {
    if(is_page($pid))
   {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
        //$cid = $cidArray[$pid];
     }else{
        $cid = "";
     }
 }else{
     $cid = "";
 }

return <<<EOL
<form id="geo_lp_uk" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="first_name_uk" maxlength="40" name="first_name" size="20" type="text" placeholder="First Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input  id="last_name_uk" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email_uk" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="phone_uk" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<input  id="company_uk" maxlength="40" name="company" size="20" type="text" class="textfield" placeholder="Company:"/>
<textarea class="textfield" id="description_uk" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn_uk" value="Contact Us Today">
</form>
EOL;
}
add_shortcode('sf_geo_uk','geo_lpa_uk');



/*==========================
LP GEO US
===========================*/
function geo_lpa_us() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();  
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

 $dataSet = array();
 $cidArray = array(
    '900' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalrendering-lpa/'), 
    '901' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalanimation-lpa/'),    
    '902' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalvisualization-lpa'), 
    '903' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3danimation-lpa/ '), 
    '904' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3drendering-lpa/'),
    '905' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3dvisualization-lpa/'),
    '906' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3dmodeling-lpa/'),
    '907' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2danimation-lpa/'),
    '908' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2drendering-lpa/'),
    '909' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2dvisualization-lpa/'),
    '910' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2dmodeling-lpa/'),
    '911' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalrendering-lpb/'),
    '912' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalanimation-lpb/'),
    '913' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-architecturalvisualization-lpb/'),
    '914' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3danimation-lpb/'),
    '915' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3drendering-lpb/'),
    '916' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3dvisualization-lpb/'),
    '917' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-3dmodeling-lpb/'),
    '918' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2danimation-lpb/ ‎'),
    '919' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2drendering-lpb/'),
    '920' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2dvisualization-lpb/'),
    '921' =>  array('cid' => '701900000012U6G', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/us-2dmodeling-lpb/'));
 
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray))
 {
    if(is_page($pid))
   {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
        //$cid = $cidArray[$pid];
     }else{
        $cid = "";
     }
 }else{
     $cid = "";
 }

return <<<EOL
<form id="geo_lp_us" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data2" />
<div>
  <input id="first_name_us" maxlength="40" name="first_name" size="20" type="text" placeholder="First Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input  id="last_name_us" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last Name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email_us" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="phone_us" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<input  id="company_us" maxlength="40" name="company" size="20" type="text" class="textfield" placeholder="Company:"/>
<textarea class="textfield" id="description_us" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn_us" value="Contact Us Today">
</form>
EOL;
}
add_shortcode('sf_geo_us','geo_lpa_us');

function treatment_lp_b() {
    $getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();
$keyword = fetchKeyword(get_query_var('keyword'));   
$leadsource = displayDDLeadSource(get_query_var('leadsource')); 
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
    
    COUNTRY: $info->country
    CITY: $info->city
    REGION: $info->regionName
    TIMEZONE: $info->timezone
    COUNTRY CODE: $info->countryCode
    AREA CODE: $areacode
    IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;

return <<<EOL
<form id="cc_lp_form" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="701900000011hGD" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="Animation1 - Adwords - Treatment - B">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="http://animation1.com/3dservice-lpb/" />
<div>
  <input id="first_name2" maxlength="40" name="first_name" size="20" type="text"  value="First Name:" />
  <input  id="last_name2" maxlength="80" name="last_name" size="20" type="text"   value="Last Name:" />
</div>
<div>
<input  id="email2" maxlength="80" name="email" size="20" type="text" class="textfield" value="Email:"/>  
<input  id="phone2" maxlength="40" name="mobile" size="20" type="text" class="textfield" value="Phone:"/>
<input  id="company2" maxlength="40" name="company" size="20" type="text" class="textfield" value="Company:"/>
<textarea class="textfield" id="description2" name="description">Message:</textarea>
$trackinfo
<input type="submit" name="submit" class="contactBtn2" value="Contact Us Today">
</form>

EOL;
}
add_shortcode('sf_form_treatment_b','treatment_lp_b');
/*==============================
    AUGMENTED REALITY LP
==============================*/
function augmented_LP() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();  
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
COUNTRY: $info->country
CITY: $info->city
REGION: $info->regionName
TIMEZONE: $info->timezone
COUNTRY CODE: $info->countryCode
AREA CODE: $areacode
IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;
 $dataSet = array();
 $cidArray = array(
    '933' =>  array('cid' => '701900000012MZw', 'title' => 'Animation1 - Adwords - GEO Target',  'url' => 'http://animation1.com/au-augmented-lpa/'),
    '935' =>  array('cid' => '701900000012MZw', 'title' => 'Animation1 - Adwords - GEO Target',  'url' => 'http://animation1.com/us-augmented-lpa/'),
    '936' =>  array('cid' => '701900000012MZw', 'title' => 'Animation1 - Adwords - GEO Target', 'url' => 'http://animation1.com/uk-augmented-lpa/'));
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray)) {
    if(is_page($pid)) {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
    } else {
           $cid = "";
    }
    }else {
            $cid = "";
}
return <<<EOL
<form id="augmented_lp" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />
<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="fname" maxlength="40" name="first_name" size="20" type="text" placeholder="First Name:" style='width: 50%;float: left; border-right:0;border-radius: 3px 0 0 3px;' />
  <input id="lname" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last Name:" style='width: 50%;float: left; border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="mobile" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<textarea class="textfield" id="message" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="augmented_btn" value="Let's get started!">
</form>
EOL;
}
add_shortcode('sf_augmentedlp','augmented_LP');


/*=============================
    MOBILEAPP LP
==============================*/
function mobileapp_au() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();  
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
COUNTRY: $info->country
CITY: $info->city
REGION: $info->regionName
TIMEZONE: $info->timezone
COUNTRY CODE: $info->countryCode
AREA CODE: $areacode
IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;
 $dataSet = array();
 $cidArray = array(
    '932' =>  array('cid' => '701900000012r4g', 'title' =>'Animation1 - Adwords - Mobile App', 'url' => 'http://animation1.com/au-mobileapp-lpa/'),
    '937' =>  array('cid' => '701900000012r4g', 'title' =>'Animation1 - Adwords - Mobile App', 'url' => 'http://animation1.com/uk-mobileapp-lpa/'),
    '938' =>  array('cid' => '701900000012r4g', 'title' =>'Animation1 - Adwords - Mobile App', 'url' => 'http://animation1.com/us-mobileapp-lpa/'),
    '989' =>  array('cid' => '701900000012r4g', 'title' =>'Animation1 - Adwords - Mobile App', 'url' => 'http://animation1.com/au-mobileapp-v2/'),
    '991' =>  array('cid' => '701900000012r4g', 'title' =>'Animation1 - Adwords - Mobile App', 'url' => 'http://animation1.com/us-mobileapp-v2/'));
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray)) {
    if(is_page($pid)) {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
    } else {
           $cid = "";
    }
    }else {
            $cid = "";
}
return <<<EOL
<form id="mobileapp_au" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />

<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="fname" maxlength="40" name="first_name" size="20" type="text" placeholder="First name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input id="lname" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="mobile" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<textarea class="textfield" id="message" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="mobileapp_btn" value="Let's get started!">
</form>
EOL;
}
add_shortcode('sf_mobileapp_au','mobileapp_au');



/*=============================
    AUGMENTED LPB
==============================*/
function augmented_lpb() {
$getIP = GetIP();
$info = fetchLocation();
$areacode = "(".getAreacode($info->countryCode).")";
$referrer = getReferer();  
$keyword = fetchKeyword(get_query_var('keyword'));  
$leadsource = displayDDLeadSource(get_query_var('leadsource'));
$trackinfo = <<<TA
<textarea class="trackinfo" style="display: none;" id="00N9000000CW9TC" name="00N9000000CW9TC" rows="3" type="text" wrap="soft">
COUNTRY: $info->country
CITY: $info->city
REGION: $info->regionName
TIMEZONE: $info->timezone
COUNTRY CODE: $info->countryCode
AREA CODE: $areacode
IP: $getIP
</textarea>
<textarea  id="00N9000000DGxIA" name="00N9000000DGxIA" rows="3" type="text" wrap="soft" style="display: none;">
    $referrer
</textarea>
<textarea  id="00N9000000DGxMN" name="00N9000000DGxMN" rows="3" type="text" wrap="soft" style="display: none;">
    $keyword
</textarea>
TA;
 $dataSet = array();
 $cidArray = array(
    '990' =>  array('cid' => '701900000012MZw', 'title' => 'Animation1 - Adwords - Augmented Reality', 'url' => 'http://animation1.com/au-augmented-lpb/'),
    '992' =>  array('cid' => '701900000012MZw', 'title' => 'Animation1 - Adwords - Augmented Reality', 'url' => 'http://animation1.com/us-augmented-lpb/'));
 $pid = get_the_ID();
 if(array_key_exists($pid,$cidArray)) {
    if(is_page($pid)) {
    foreach($cidArray as $key => $value) {
        if($key == $pid) {
            $dataSet[] = $value;
            $data1 = $dataSet[0]['cid'];
            $data2 = $dataSet[0]['title'];
            $data3 = $dataSet[0]['url'];
        }
    }
    } else {
           $cid = "";
    }
    }else {
            $cid = "";
}
return <<<EOL
<form id="mobileapp_au" action="" method="POST">
<input type=hidden name="oid" value="00D90000000sLJt">
<input type=hidden name="retURL" value="http://animation1.com/ty">
<input type="hidden" name="Campaign_ID" value="$data1" />
<input type="hidden" name="member_status" value="Responded" />
<input  id="city" maxlength="40" name="city" size="20" type="hidden" value="$info->city" />
<input  id="state" maxlength="20" name="state" size="20" type="hidden" value="$info->region" />
<input  id="country" maxlength="40" name="country" size="20" type="hidden" value="$info->country" />
<input  id="phone" maxlength="40" name="phone" size="20" type="hidden" value="$areacode" />
$leadsource
<input type="hidden" name="Owner" value="AN1_WebLeads" />

<input type="hidden" id="00N9000000CVANp" name="00N9000000CVANp" value="$data2">
<input  id="00N9000000CW2eA" maxlength="255" name="00N9000000CW2eA" size="20" type="hidden" value="$data3" />
<div>
  <input id="fname" maxlength="40" name="first_name" size="20" type="text" placeholder="First name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-left: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-right:0;border-radius: 3px 0 0 3px;' />
  <input id="lname" maxlength="80" name="last_name" size="20" type="text"  placeholder="Last name:" style='width: 50%;float: left;border-top: 1px solid #AEACAC;border-right: 1px solid #AEACAC;border-bottom: 1px solid #AEACAC;border-left: 0;border-radius: 0 3px 3px 0;' />
</div>
<div>
<input  id="email" maxlength="80" name="email" size="20" type="text" class="textfield" placeholder="Email:"/>  
<input  id="mobile" maxlength="40" name="mobile" size="20" type="text" class="textfield" placeholder="Phone:"/>
<textarea class="textfield" id="message" name="description" placeholder="Message:"></textarea>
$trackinfo
<input type="submit" name="submit" class="mobileapp_btn" value="Let's get started!">
</form>
EOL;
}
add_shortcode('sf_augmented_lpb','augmented_lpb');

function sales_style() {
    wp_register_style("sales-style", WP_PLUGIN_URL."/Sales-Force-Plugin/sf_webforms.css");
    wp_enqueue_style("sales-style");
}

add_action('wp_enqueue_scripts','sales_style');

function sales_force_script() {
    //wp_enqueue_script("jquery");
    //wp_register_script('sales-validate','http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js',"","1.11.1",false);
    //wp_enqueue_script('sales-validate');
    //wp_enqueue_script("sales-validate", WP_PLUGIN_URL."/Sales-Force-Plugin/jquery.validate.min.js","","1.6",1);
    wp_register_script("sales-force", WP_PLUGIN_URL."/Sales-Force-Plugin/sf_webform.js","","1.3.1",true);
    wp_enqueue_script("sales-force");
}
add_action('wp_enqueue_scripts','sales_force_script');

/*==========================
KEYWORD FETCH
==========================*/
function fetchKeyWord($keyword)
{
    if(isset($keyword))
    {
        return $keyword;
    }
    return "N/A";
}

?>