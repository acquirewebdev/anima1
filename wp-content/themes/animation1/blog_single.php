<?php 
/**
* Template Name Posts: Blog Single 
*/
get_header(); ?>
<section id="blog-single">
	<div class="resetPadding">
		<div class="wrapper">
			<div class="blog-wrap">
				<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
					<div class="blog-box">
					
						<?php if(have_posts()) : ?>
						<?php while(have_posts()) : the_post(); ?>

						<div class="col-md-12 resetPadding">
							<div class="post-box">

								<?php /*<?php if(function_exists('has_post_thumbnail') && has_post_thumbnail()) : ?>
									<?php echo the_post_thumbnail($post->ID); ?>
								<?php else: ?>
									<?php main_image(); ?>
								<?php endif;  */ ?>

								<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8 resetPadding">
									<div class="post-title">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

									</div>
								</div>
								<div class="col-md-4 col-lg-4 col-xs-4 col-xs-4 pull-right resetPadding">
									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
										<div class="post-like">
											<div class="like">
												<?php
													if(function_exists('like_counter_p')) { like_counter_p(''); }
												?>
											</div>
											<div class="dislike">
												<?php
													if(function_exists('dislike_counter_p')) { dislike_counter_p(''); }
												?>
											</div>
										</div>
									</div>
									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
									<!-- Go to www.addthis.com/dashboard to customize your tools -->
									<div style="float: right; margin: 5px 0 0 0;" class="addthis_sharing_toolbox"></div>									
									</div>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 resetPadding">
									<span class="post-date">
										<?php the_time(get_option('date_format')); ?> - Posted by: <span class="orange"><?php the_author(); ?></span> to <?php foreach((get_the_category()) as $category) { $category->cat_name . ' '; } ?>
										<a href="<?php echo get_category_link(get_cat_id($category->cat_name)); ?>"><?php echo $category->cat_name ?></a>
									</span>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 resetPadding">
									<div class="post-content-single"><?php the_content(); ?></div>
								</div>
							</div>
							<!--disqus comment form-->
							<?php disqus_embed('animation1-blog'); ?>
							<!--end disqus-->
						</div>
						<?php 
						endwhile; endif;  
						//blog_pagination();
						?>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
					<div class="sidebar-box">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>