<?php 
/*
* Template Name: 3D ANIMATION
*/
?>
<?php get_header(); ?>

<section id="animation-banner-inner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content-inner">
					<h1 class="white">3D ANIMATIONS</h1>
					<div class="menu-top-menu-wrap">
							<?php 
								/*
								$args = array(
								    'menu'    => 'top-menu',
								    'submenu' => '3D Animations',
								);
								wp_nav_menu( $args );
								*/
							?>
						<!-- <div class="bannerbtn">
							<a class="text-center " href="<?php bloginfo('url'); ?>/contact-us/">Get started</a>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- <section id="animation3d">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1>With one of the largest rendering farms in the region, Animation1 has the edge in effectively communicating your vision through stunning 3D-animated artwork.</h1>
				<?php //echo base64('video-thumb2.jpg'); ?>
				<div class="video-vimeo">
					<div class='embed-container'><iframe src='https://player.vimeo.com/video/124284972' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<section id="animation-two">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 resetPadding">
						<div class="services-right-inner">
								<?php echo base64('3d-walkthrough.jpg'); ?>
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 resetPadding">
						<div class="services-left-inner">
							<h1 class="orange">3D walkthroughs</h1>
							<p>We create accurate 3D animated walkthroughs of all types of projects, showcasing various perspectives and covering all your specifications to the last detail. </p>
							
						<div class="animation-inner-sub-works style-1 owlElement">
						<?php 
							$cat = 30;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="list-works">
							<div class="itemz view third-effect-black">
								<?php 
								//echo the_title();
								
									if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
										echo base64d($video_thumbnail,'animation1 video',''); 
									} 
								?>
								<div class="works-mask">
									<a class="fancybox fancybox.iframe" href="<?php echo the_field('video-link'); ?>">	<?php echo base64('view-img2.png','','view-item');  ?></a>
								</div>
								<div class="animation-works-title">
									<h2><?php the_title(); ?></h2>
								</div>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
						</div>
						<div style="max-width: 661px; margin: 10px auto 0; padding: 0;">
							<a style="background-color: #ff8500; color: #fff; max-width: 168px; width: 100%; text-align: center; float:right; padding: 10px 0; font-size: 1.1em; border-radius: 5px; -o-border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px;" class="getstarted text-right" href="#">View all projects</a>
						</div>
						<div class="customNavigation">
							<a class="btn-prev"><?php base64('btn-prev-grey.jpg','',''); ?></a>
							<a class="btn-next"><?php base64('btn-next-grey.jpg','',''); ?></a>
						</div>
						
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section id="animation-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 resetPadding">
						<div class="services-right-inner">
							<?php echo base64('3d-flythroughs.jpg'); ?>
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 resetPadding">
						<div class="services-left-inner">
							<h1 class="orange">3D flythroughs</h1>
							<p>We can showcase your projects in 360 degree view, from the ground, street level, and from above via drone images.</p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('services-inner-btn'); ?></a> -->
							
							<div class="animation-inner-sub-works style-1 owlElement">
							<?php 
							$cat = 31;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
							?>
							<div class="list-works">
							<div class="itemz view third-effect-black">
								<?php 
								//echo the_title();
								
									if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
										echo base64d($video_thumbnail,'animation1 video',''); 
									} 
								?>
								<div class="works-mask">
									<a class="fancybox fancybox.iframe" href="<?php echo the_field('video-link'); ?>">	<?php echo base64('view-img2.png','','view-item');  ?></a>
								</div>
								<div class="animation-works-title">
									<h2><?php the_title(); ?></h2>
								</div>
							</div>
							</div>
							<?php
								wp_reset_query();
								endwhile;
							endif;	
							?>
							</div>
							<div style="max-width: 661px; margin: 10px auto 0; padding: 0;">
							<a style="background-color: #ff8500; color: #fff; max-width: 168px; width: 100%; text-align: center; float:right; padding: 10px 0; font-size: 1.1em; border-radius: 5px; -o-border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px;" class="getstarted text-right" href="#">View all projects</a>
							</div>
							<div class="customNavigation">
							<a class="btn-prev"><?php base64('btn-prev-grey.jpg','',''); ?></a>
							<a class="btn-next"><?php base64('btn-next-grey.jpg','',''); ?></a>
							</div>

							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="animation-four">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 resetPadding">
						<div class="services-right-inner">
							<?php echo base64('product-demos.jpg'); ?>
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 resetPadding">
						<div class="services-left-inner">
							<h1 class="orange">Product demos</h1>
							<p>Using the latest 3D animation technology in the market, we create and animate crisp, realistic visualisations highlighting your product features and benefits in fine detail.</p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('services-inner-btn'); ?></a> -->
						
							<div class="animation-inner-sub-works style-1 owlElement">
						<?php 
							$cat = 32;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="list-works">
							<div class="itemz view third-effect-black">
								<?php 
								//echo the_title();
								
									if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
										echo base64d($video_thumbnail,'animation1 video',''); 
									} 
								?>
								<div class="works-mask">
									<a class="fancybox fancybox.iframe" href="<?php echo the_field('video-link'); ?>">	<?php echo base64('view-img2.png','','view-item');  ?></a>
								</div>
								<div class="animation-works-title">
									<h2><?php the_title(); ?></h2>
								</div>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
						</div>
						<div style="max-width: 661px; margin: 10px auto 0; padding: 0;">
							<a style="background-color: #ff8500; color: #fff; max-width: 168px; width: 100%; text-align: center; float:right; padding: 10px 0; font-size: 1.1em; border-radius: 5px; -o-border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px;" class="getstarted text-right" href="#">View all projects</a>
						</div>
						<div class="customNavigation">
							<a class="btn-prev"><?php base64('btn-prev-grey.jpg','',''); ?></a>
							<a class="btn-next"><?php base64('btn-next-grey.jpg','',''); ?></a>
						</div>

						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section id="animation-six">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="inner-title">
					<h1>Work with Animation1 as your partner and get the following benefits:</h1>
					<hr />
				</div>
				<div id="animation-six-wrap">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
						<div class="benefits-box">
							<?php echo base64('present.png'); ?>
							<h2>Present your value proposition creatively</h2>
							<p>Of all online users, 58% want to see video content like explainer videos and product demos. Increase your audience engagement with targeted 3D animated presentations. </p>
						</div>
					</div>
					
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
						<div class="benefits-box">
							<?php echo base64('progress.png'); ?>
							<h2>Leading animation team</h2>
							<p>Our full-time, in-house team of artists and professionals lends their extensive experience in animation, with specialists from each industry vertical we service managing production and quality assurance. </p>
						</div>
					</div>
					
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
						<div class="benefits-box">
							<?php echo base64('money.png'); ?>
							<h2>3D animations at a competitively low cost </h2>
							<p>Our operational scale lets us offer our 3D animation services at a competitively low market price, making world-class animations accessible for your budget. </p>
						
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="animation-five">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-center resetPadding">
					<div class="ideas" class="text-center">
						<h3 class="inline-block">Tell us about your project</h3>
						<a class="getstarted inline-block" href="">Get started</a>
						<div id="form" style="display: none;">
							asd
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>
<?php /*
<section id="animation-works">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<h1 class="orange text-center">Some of our works</h1>
			<div id="animation-works-wrap">	
				<div class="wrapper">

						<div class="animation-inner-sub-works">
						<?php 
							if (is_page(76) ) :
								$cat = 24; //Architectural
							elseif (is_page(162) ) :
								$cat = 9; //Infrastructure
							elseif (is_page(164) ) :
								$cat = 10; //Medical
							elseif (is_page(166) ) :
								$cat = 11; //Forensics
							elseif (is_page(168) ) :
								$cat = 12; //Entertainment & Cinema
							endif;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="list-works">
							<div class="itemz view third-effect-black">
								<?php 
								//echo the_title();
								
									if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
										echo base64d($video_thumbnail,'animation1 video',''); 
									} 
								?>
								<div class="works-mask">
									<a class="fancybox fancybox.iframe" href="<?php echo the_field('video-link'); ?>">	<?php echo base64('view-img.png','','view-item');  ?></a>
								</div>
								<div class="animation-works-title">
									<h2><?php the_title(); ?></h2>
								</div>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
						</div>
					<div class="customNavigation">
						<a class="btn-prev"><?php base64('btn-prev.jpg','',''); ?></a>
						<a class="btn-next"><?php base64('btn-next.jpg','',''); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
*/ ?>
<!-- <section id="animation-five">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div id="services-inner-box">
						<h1>Bring your ideas to life and let your prospects visualise your<br /> project from all angles with 3D animation</h1>
						<div class="animation-inner-btn-large-wrap">
							<a class="text-center animation-inner-btn-large" href="<?php echo bloginfo('url'); ?>/contact-us/">Get Started</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<?php get_footer(); ?>