<?php 
/*
* Template Name: Industry Page
*/
?>
<?php get_header(); ?>

<section class="single-banner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content">
					<h1 class="white"><?php the_title(); ?></h1>
						<p class="white">
							<span class="orange sourcebold">Experienced artists and professionals work</span> together to create the most advanced visualisation solutions   meeting the standards of a wide variety of industries.
						</p>
						<div class="box-banner">
							<a href="<?php echo bloginfo('url'); ?>/contact-us/">GET STARTED</a>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1><?php the_field('industry-h1'); ?></h1>
				<p><?php the_field('industry-p'); ?></p>
				<?php $image = get_field('industry-img'); 
					if(!empty($image)) : ?>
					<?php echo base64d($image['url'], 'industry image'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>


<section id="industry-one">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-left top49">
							<h1 class="orange"><?php the_field('industry-one-h1'); ?></h1>
							<p><?php the_field('industry-one-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('industry-btn'); ?></a> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-right">
								<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry-two">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-left ">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-right">
							<h1 class="orange"><?php the_field('industry-two-h1'); ?></h1>
							<p><?php the_field('industry-two-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('industry-btn'); ?></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-left">
							<h1 class="orange"><?php the_field('industry-three-h1'); ?></h1>
							<p><?php the_field('industry-three-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('industry-btn'); ?></a> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-right">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry-four">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-left">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-right">
							<h1 class="orange"><?php the_field('industry-four-h1'); ?></h1>
							<p><?php the_field('industry-four-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('industry-btn'); ?></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry-five">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-left">
							<h1 class="orange"><?php the_field('industry-five-h1'); ?></h1>
							<p><?php the_field('industry-five-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('industry-btn'); ?></a> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-right">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php get_footer(); ?>