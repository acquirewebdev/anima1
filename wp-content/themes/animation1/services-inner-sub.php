<?php 
/*
* Template Name: Servies Inner Sub Page
*/
?>
<?php get_header(); ?>
<section class="single-banner-inner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content-inner">
					<h1 class="white"><?php the_title(); ?></h1>
						<div class="menu-top-menu-wrap">
							<?php 
								$args = array(
								    'menu'    => 'top-menu',
								    'submenu' => '3D Animations',
								);
								wp_nav_menu( $args );
							?>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="services-inner-sub">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1><?php the_field('services-inner-sub-h1'); ?></h1>
				<p><?php the_field('services-inner-sub-p'); ?></p>

				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
					<div class="services-inner-sub-images">
						<?php $image = get_field('services-inner-sub-one-img'); 
							if(!empty($image)) : ?>
							<?php echo base64d($image['url'], 'industry image'); ?>
						<?php endif; ?>
						<div class="services-inner-sub-box">
							<h1 class="orange"><?php the_field('services-inner-sub-one-h1'); ?></h1>
							<p><?php the_field('services-inner-sub-one-p'); ?></p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
					<div class="services-inner-sub-images">
						<?php $image = get_field('services-inner-sub-two-img'); 
							if(!empty($image)) : ?>
							<?php echo base64d($image['url'], 'industry image'); ?>
						<?php endif; ?>
						<div class="services-inner-sub-box">
							<h1 class="orange"><?php the_field('services-inner-sub-two-h1'); ?></h1>
							<p><?php the_field('services-inner-sub-two-p'); ?></p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
					<div class="services-inner-sub-images">
						<?php $image = get_field('services-inner-sub-three-img'); 
							if(!empty($image)) : ?>
							<?php echo base64d($image['url'], 'industry image'); ?>
						<?php endif; ?>
						<div class="services-inner-sub-box">
							<h1 class="orange"><?php the_field('services-inner-sub-three-h1'); ?></h1>
							<p><?php the_field('services-inner-sub-three-p'); ?></p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<section id="industry-works">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<h1 class="orange text-center">SOME OF OUR WORKS</h1>
			<div id="industry-works-wrap">	
				<div class="wrapper">
					<div class="services-inner-sub-works">
						<?php 
							if (is_page(182) ) :
								$cat = 5; //3d rendering & animation
							elseif (is_page(184) ) :
								$cat = 6; //2d illustration & animation
							elseif (is_page(188) ) :
								$cat = 7; //stereo 3d production
							endif;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="itemz view third-effect-black">
							<?php 
							//echo the_title();
							
								if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
									echo base64d($video_thumbnail,'animation1 video',''); 
								} 
							?>
							<div class="mask">
								<a href="<?php echo bloginfo('url'); ?>" target="_new" class=""><?php echo base64('view-img.png','','view-item');  ?></a>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
					</div>
					<div class="customNavigation">
						<a class="btn-prev"><?php base64('btn-prev.jpg','',''); ?></a>
						<a class="btn-next"><?php base64('btn-next.jpg','',''); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="industry-inner-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
					<div id="industry-inner-box">
						<h1><?php the_field('services-inner-sub-four-h1'); ?></h1>
						<p><?php the_field('services-inner-sub-four-p'); ?></p>
						<div class="industry-inner-btn-large-wrap">
							<a class="text-center services-inner-btn-large" href="<?php echo bloginfo('url'); ?>/contact-us/"><?php the_field('services-inner-sub-btn-large'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>