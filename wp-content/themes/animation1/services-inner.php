<?php 
/*
* Template Name: Services Inner Page
*/
?>
<?php get_header(); ?>

<section class="single-banner-inner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content-inner">
					<h1 class="white"><?php the_field('page-title'); ?></h1>
						<div class="menu-top-menu-wrap">
							<?php 
								/*
								$args = array(
								    'menu'    => 'top-menu',
								    'submenu' => '3D Animations',
								);
								wp_nav_menu( $args );
								*/
							?>
							<div class="banner-btn">
								<a class="text-center " href="">Get started</a>
							</div>
						</div>
					

				</div>
			</div>
		</div>
	</div>
</section>
<section id="services-inner">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1><?php the_field('services-inner-h1'); ?></h1>
				<p><?php the_field('services-inner-p'); ?></p>
				<?php $image = get_field('services-inner-img'); 
					if(!empty($image)) : ?>
					<?php echo base64d($image['url'], 'services image'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<section id="services-inner-two">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="services-left-inner top49">
							<h1 class="orange"><?php the_field('services-inner-two-h1'); ?></h1>
							<p><?php the_field('services-inner-two-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('services-inner-btn'); ?></a> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="services-right-inner">
								<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="services-inner-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="services-left-inner">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="services-right-inner top49">
							<h1 class="orange"><?php the_field('services-inner-three-h1'); ?></h1>
							<p><?php the_field('services-inner-three-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('services-inner-btn'); ?></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="services-inner-four">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 services-left-inner">
							<h1 class="orange"><?php the_field('services-inner-four-h1'); ?></h1>
							<p><?php the_field('services-inner-four-p'); ?></p>
							<!-- <a class="text-right pull-right services-inner-btn" href=""><?php the_field('services-inner-btn'); ?></a> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="services-right-inner">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="services-inner-five">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="services-inner-wrap">
					<div id="services-inner-box">
						<h1><?php the_field('services-inner-five-h1'); ?></h1>
						<p><?php the_field('services-inner-five-p'); ?></p>
						<div class="services-inner-btn-large-wrap">
							<a class="text-center services-inner-btn-large" href="<?php echo bloginfo('url'); ?>/contact-us/"><?php the_field('services-inner-btn-large'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>