<?php

/*=====================================================
GOOGLE ANALYTICS
=====================================================*/
add_action('wp_footer', 'add_googleanalytics');
function add_googleanalytics() {
?>
<!--analytics-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-51169252-1', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script>
<!--end analytics-->
<?php
}

/*=====================================================
ADROLL CODES
=====================================================*/
add_action('wp_footer', 'add_adroll');
function add_adroll() {
?>
<!--adroll-->
<script type="text/javascript">
adroll_adv_id = "QD3KENDYK5BFZH6HQX7GC3";
adroll_pix_id = "EFLORJNB2VANTPYRIIK2EF";
(function () { 
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true; 
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}()); 
</script>
<!--end adroll-->
<?php
}

/*=====================================================
DISQUS COMMENT FORM
=====================================================*/
function disqus_embed($disqus_shortname) {
    global $post;
    wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
    echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}
/*=====================================================
DISQUS COMMENT COUNT
=====================================================*/
add_action('wp_footer', 'disqus_count');
 function disqus_count() {
?>
 <script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
     var disqus_shortname = 'animation1-blog';
    /* * * DON'T EDIT BELOW THIS LINE * * */
     (function () {
         var s = document.createElement('script'); s.async = true;
         s.type = 'text/javascript';
         s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
     }());
 </script>

<?php
}

// Disqus: Prevent from replacing comment count
remove_filter('comments_number', 'dsq_comments_text');
remove_filter('get_comments_number', 'dsq_comments_number');
remove_action('loop_end', 'dsq_loop_end');


/*=====================================================
CUSTOM POST TYPE
=====================================================*/
// add_action( 'init', 'create_post_type' );
// function create_post_type() {
//   register_post_type( 'single-blog',
//     array(
//       'labels' => array(
//         'name' => __( 'blog' ),
//         'singular_name' => __( 'blog' )
//       ),
//       'public' => true,
//       'has_archive' => true,
//     )
//   );
// }

/*=====================================================
COPYRIGHT
=====================================================*/
function copyright() {
    $oneYearOn = date('Y',strtotime(date('Y', mktime()) . " + 365 day")); 
    $today = date('Y'). " - ". $oneYearOn;
    echo "<p>&copy; All rights reserved for <span class=orange>ANIMATION1</span> $today</p>";
}

/*=====================================================
POST THUMBNAIL
=====================================================*/
if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails',array('post')); //for post only remove if you want it also in page
set_post_thumbnail_size( 296, 181, true ); // default Post Thumbnail dimensions (cropped)
}


/*======================================================
POST THUMBNAIL OG IMAGE
======================================================*/
//function to call first uploaded image in functions file
function og_image() {
$files = get_children('post_parent='.get_the_ID().'&post_type=attachment
&post_mime_type=image&order=desc');
  if($files) :
    $keys = array_reverse(array_keys($files));
    $j=0;
    $num = $keys[$j];
    $image=wp_get_attachment_image($num, 'large', true);
    $imagepieces = explode('"', $image);
    $imagepath = $imagepieces[1];
    $main=wp_get_attachment_url($num);
        $template=get_template_directory();
        $the_title=get_the_title();
    print "$main";
   else :
    print '$main';   
   endif;
}

/*======================================================
POST THUMBNAIL FALLBACK
======================================================*/
//function to call first uploaded image in functions file
function main_image() {
$files = get_children('post_parent='.get_the_ID().'&post_type=attachment
&post_mime_type=image&order=desc');
  if($files) :
    $keys = array_reverse(array_keys($files));
    $j=0;
    $num = $keys[$j];
    $image=wp_get_attachment_image($num, 'large', true);
    $imagepieces = explode('"', $image);
    $imagepath = $imagepieces[1];
    $main=wp_get_attachment_url($num);
        $template=get_template_directory();
        $the_title=get_the_title();
    print "<img src='$main' alt='$the_title' class='frame' />";
   else :
    print '<img src="'.get_bloginfo('template_directory'). '/images/no-image.jpg'.'" alt="thumbnail" />';   
   endif;
}

/*function excerpt length*/
function new_excerpt_length($length) {
    return 35;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/*======================================================
MY FUNCTIONS SCRIPT
======================================================*/
function my_render_script() {

if( !is_admin() && is_front_page()){
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"), '', '2.1.3',true);
	wp_enqueue_script('jquery');
}
else {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), '', '1.9.1',true);
    wp_enqueue_script('jquery');
    
}

wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js','','3.3.4',true);
wp_enqueue_script('bootstrap');

wp_register_script('modernizr', get_template_directory_uri(). '/js/modernizr.custom.26633.js','','2.8.3',true);
wp_enqueue_script('modernizr');

wp_register_script('waypoints', get_template_directory_uri(). '/js/waypoints.min.js','','2.0.3',true);
wp_enqueue_script('waypoints');

wp_register_script('lazyload-js', get_template_directory_uri(). '/js/lazyload.min.js','','1.9.5',true);
wp_enqueue_script('lazyload-js');

wp_register_script('jig-js', get_template_directory_uri(). '/js/jquery.gridrotator.js','','1.1.0',true);
wp_enqueue_script('jig-js');

if(is_front_page() ) :
wp_register_script('carousel2-js', get_template_directory_uri(). '/js/owl.carousel2.min.js','','2.0.0',true);
wp_enqueue_script('carousel2-js');
else :
wp_register_script('carousel-js', get_template_directory_uri(). '/js/owl.carousel.js','','1.0.0',true);
wp_enqueue_script('carousel-js');
endif;

wp_register_script('fancyapps-js', get_template_directory_uri(). '/js/jquery.fancybox.pack.js','','2.0.0',true);
wp_enqueue_script('fancyapps-js');


// wp_register_script('bx-slider-js', get_template_directory_uri(). '/js/jquery.bxslider.min.js','','4.1.2',true);
// wp_enqueue_script('bx-slider-js');

wp_register_script('script', get_template_directory_uri(). '/js/script.js','','1.0',true);
wp_enqueue_script('script');

}   
add_action('wp_enqueue_scripts', 'my_render_script', 1);
/*======================================================
MY FUNCTIONS CSS
======================================================*/
function my_render_style() {

wp_register_style('bootstrap', get_template_directory_uri(). '/css/bootstrap.min.css');
wp_enqueue_style('bootstrap');

if(is_front_page() ) :
wp_register_style('carousel2', get_template_directory_uri(). '/css/owl.carousel2.css');
wp_enqueue_style('carousel2');
else :
wp_register_style('carousel1', get_template_directory_uri(). '/css/owl.carousel1.css');
wp_enqueue_style('carousel1');
endif;

wp_register_style('animate', get_template_directory_uri(). '/css/animate.css');
wp_enqueue_style('animate');

wp_register_style('fontawesome', get_template_directory_uri(). '/font-awesome/css/font-awesome.min.css');
wp_enqueue_style('fontawesome');

wp_register_style('hover', get_template_directory_uri(). '/css/hover.css');
wp_enqueue_style('hover');

wp_register_style('fancybox', get_template_directory_uri(). '/css/fancybox/jquery.fancybox.css');
wp_enqueue_style('fancybox');

// wp_register_style('bx-slider-css', get_template_directory_uri(). '/css/jquery.bxslider.css');
// wp_enqueue_style('bx-slider-css');

}
add_action('wp_enqueue_scripts','my_render_style');
/*=====================================================
Register custom menu in wordpress theme
=====================================================*/
function my_menu() {
    register_nav_menus(
        array(
            'header-menu'=>__('Header Menu'),
            'footer-menu'=>__('Footer Menu')
            )
        );
}
add_action('init','my_menu');
/*====================================================
Display Home Menu
====================================================*/
function my_page_menu_args( $args ) {
        $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'my_page_menu_args' );
/*====================================================
REMOVE META TAGS FOR VC AND WORDPRESS
====================================================*/
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

// add_action('init', 'myoverride', 100);
// function myoverride() {
//     remove_action('wp_head', array(visual_composer(), 'addMetaData'));
// }
/*===================================================
BASE64 IMAGE
===================================================*/
function fileExt($img_src) {
    $image = explode(".",$img_src);
    $ext   = end($image);
    return $ext;
}
function base64($img_src, $altext=false, $class=false) {
    //echo dirname(__FILE__) .;
    //define('ABSPATH', 'wp-content/themes/animation1/images' .)
    //$root = bloginfo('template_directory');
    //echo get_home_path();
    //echo $root;
    $ext = fileExt($img_src);
    $root  = get_stylesheet_directory();
    $image = "images";
    $data  = "data:image/";
    $datas = $data.$ext.";base64";
    $url = $root."/".$image."/".$img_src;
    $imgbinary = fread(fopen($url, "r"), filesize($url));
    $img_str = base64_encode($imgbinary);
    echo '<img src="'.$datas.','.$img_str.'" alt="'.$altext.'" class="'.$class.'" />';
    //return $img_str;
}

function getFullPath($url) {
    return realpath(str_replace(get_bloginfo('url'), '.', $url));
}

function base64d($img_src, $altext=false, $class=false) {
    $ext = fileExt($img_src);
    $data  = "data:image/";
    $datas = $data.$ext.";base64";
    $path = getFullPath($img_src);
    $imgbinary = fread(fopen($path, "r"), filesize($path));
    $img_str = base64_encode($imgbinary);
    echo '<img src="'.$datas.','.$img_str.'" alt="'.$altext.'" class="'.$class.'" />';
    //return $img_str;
}



/*=====================================================
FACEBOOK META
=====================================================*/
add_action('wp_head','og_meta', 1);
function og_meta() {
?>
<?php if(is_front_page()) : ?>
<meta property="og:type" content="website" /> 
<meta property="og:image" content="http://ads.acquireap.com.s3.amazonaws.com/animation1/fb-logo.jpg" /> 
<meta property="og:title" content="3D Animation Services | Architectural Renders | Animation1" /> 
<meta property="og:url" content="<?php bloginfo('url'); ?>"/> 
<meta property="og:description" content="Animation1 is a premier visualisation studio providing 2D and 3D modeling, walkthroughs, augmented reality, interactive applications, medical and forensic animation." /> 
<meta property="og:site_name" content="<?php echo bloginfo('name'); ?>" /> 
<?php elseif (is_single()) : ?>
<meta property="og:type"   content="article" /> 
<meta property="og:url" content="<?php the_permalink(); ?>"/> 
<meta property="og:title" content="<?php single_post_title(); ?>"/> 
<meta property="og:description" content="<?php strip_tags( the_excerpt() ); ?>"/>   
<meta property="og:image" content="<?php $values = get_post_custom_values("og-img"); echo $values[0]; ?>" />
<?php endif; ?>
<?php
}

/*==================================================
ADD CLASS ON SUBMENUS
==================================================*/
add_filter('wp_nav_menu_objects' , 'my_menu_class');
function my_menu_class($menu) {
    $level = 0;
    $stack = array('0');
    foreach($menu as $key => $item) {
        while($item->menu_item_parent != array_pop($stack)) {
            $level--;
        }   
        $level++;
        $stack[] = $item->menu_item_parent;
        $stack[] = $item->ID;
        $menu[$key]->classes[] = 'level-'. ($level - 1);
    }                    
    return $menu;        
}

/*==================================================
Change Logo Login
==================================================*/
    function custom_login_logo() {
    echo '<style type="text/css">
        body { 
            background-color: #fff !important; 
        }
        h1 a {  
            background-image:url(http://ads.acquireap.com.s3.amazonaws.com/animation1/logo-admin.png) !important;
            width: 250px !important;
            margin: 0 auto !important;
            background-size: initial !important;
            height: 167px !important;
        }
        p#nav {
            display: none !important;
        }
        .login form {
            margin-top: 0 !important;
        }
        .wp-core-ui .button-primary {
            background: #ff8105 !important;
            border-color: #BE5E00 !important;
            -webkit-box-shadow: inset 0 1px 0 rgba(216, 155, 0, 0.5),0 1px 0 rgba(0,0,0,.15) !important;
            box-shadow: inset 0 1px 0 rgba(255, 208, 58, 0.5),0 1px 0 rgba(0,0,0,.15) !important;
        }
    </style>';
    }

    add_action('login_head', 'custom_login_logo');

    function my_login_logo_url() {
        return get_bloginfo( 'url' );
    }
    add_filter( 'login_headerurl', 'my_login_logo_url' );

    function add_custom_query_var( $vars ){
      $vars[] = "keyword";
      $vars[] = "leadsource";
      return $vars;
    }
    add_filter( 'query_vars', 'add_custom_query_var' );



/* REGISTER WIDGETS ------------------------------------------------------------*/

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer Section 1',
        'id'   => 'footer-one-widget',
        'description'   => 'Left Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Section 2',
        'id'   => 'footer-two-widget',
        'description'   => 'Centre Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer section 3',
        'id'   => 'footer-three-widget',
        'description'   => 'Right Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

     register_sidebar(array(
        'name' => 'Footer section 4',
        'id'   => 'footer-four-widget',
        'description'   => 'Right Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

     register_sidebar(array(
        'name' => 'Footer section 5',
        'id'   => 'footer-five-widget',
        'description'   => 'Right Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}
add_filter( 'wp_nav_menu_objects', 'submenu_limit', 10, 2 );
function submenu_limit( $items, $args ) {

    if ( empty($args->submenu) )
        return $items;

    $parent_id = array_pop( wp_filter_object_list( $items, array( 'title' => $args->submenu ), 'and', 'ID' ) );
    $children  = submenu_get_children_ids( $parent_id, $items );

    foreach ( $items as $key => $item ) {

        if ( ! in_array( $item->ID, $children ) )
            unset($items[$key]);
    }

    return $items;
}
function submenu_get_children_ids( $id, $items ) {
    $ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );
    foreach ( $ids as $id ) {
        $ids = array_merge( $ids, submenu_get_children_ids( $id, $items ) );
    }
    return $ids;
}



/*==========================
BLOG PAGINATION
===========================*/
function blog_pagination() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link('Prev') );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link('Next') );

    echo '</ul></div>' . "\n";

}
?>
