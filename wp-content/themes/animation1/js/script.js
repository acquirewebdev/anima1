$(function() {

$height = $(window).height();
$firstbackground = $("#demoreel");
$firstbackground.css({
'height' : $height
});

// $('#logo-banner-holder').on("load",function(){
//   var timerHeight = $("#logo-banner-holder").outerHeight();
//   var winHeight = (typeof window.outerHeight != 'undefined')?Math.max(window.outerHeight, $(window).height()):$(window).height();

//   var topOffset = Math.ceil((winHeight / 2) - (timerHeight / 2));
//   $("#logo-banner-holder").css({
//     'margin-top' : topOffset + 'px'
//   });
// });

// //iphone mobile get height
// var timerHeight = $("#logo-banner-holder").outerHeight();
// var winHeight = (typeof window.outerHeight != 'undefined')?Math.max(window.outerHeight, $(window).height()):$(window).height();

// var topOffset = Math.ceil((winHeight / 2) - (timerHeight / 2));
// $("#logo-banner-holder").css({
//   'margin-top' : topOffset + 'px'
// });
        
        $( '#ri-grid' ).gridrotator( {
          rows : 2,
          columns : 4,
          animType : 'fadeInOut',
          animSpeed: 700,
          maxStep : 3,
          interval : 2000,
          w1024 : {
            rows : 2,
            columns : 3
          },
          w768 : {
            rows : 3,
            columns : 3
          },
          w480 : {
            rows : 6,
            columns : 2
          },
          w320 : {
            rows : 1,
            columns : 1
          },
          w240 : {
            rows : 1,
            columns : 1
          },
        } );
      
/*=======================================================
                        LAZYLOAD
=======================================================*/
$("img.lazy").show().lazyload({
  threshold : 200,
  effect: "fadeIn"

});


/*=======================================================
                        RETINA DISPLAY
=======================================================*/
if (window.devicePixelRatio > 1) {

    var lowresImages = $('img');

    images.each(function(i) {

        var lowres = $(this).attr('src');

        var highres = lowres.replace(".", "@2x.");

        $(this).attr('src', highres);

    });

}
/*=======================================================
                        OWL CAROUSEL
=======================================================*/
 $('.owlElement2').each(function() {
      var element = $(this);
      //alert(element.attr('class'));
      if(element.hasClass('style-4')) {
      element.owlCarousel({
        autoplay: true,
        loop: true,
        autoplaySpeed: 1000,
        lazyLoad: true,
        center: true,
        margin: 0,
        responsive: {
        0: {
        items: 1
        },
        600: {
        items: 3
        },
        1000: {
        items: 5
        }
        }
    });
   }
//alert(element.attr('class'));
  if(element.hasClass('style-5')) {
      element.owlCarousel({
      autoplay: false,
      loop: true,
      speed: 2000,
      lazyLoad: true,
      center: true,
      margin: 0,
      responsive: {
        0: {
         items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    });
    if(element.find('.owl-item').length > 0) {
      $('.test-next').click(function(){
        element.trigger('next.owl.carousel');
      });
        $('.test-prev').click(function(){
        element.trigger('prev.owl.carousel');
      });
    }
     else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }
    });

  $("#industry-clients-slider").owlCarousel({
  autoplay: true,
  loop: true,
  autoplaySpeed: 1000,
  lazyLoad: true,
  center: true,
  margin: 0,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
 });

    $('.owlElement').each(function(){
      var element = $(this);
      //alert(element.attr('class'));
      if(element.hasClass('style-1')) {
        element.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [960, 3], // betweem 900px and 601px
          itemsTablet: [768,3], //2 items between 600 and 0
          itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
        });

        if(element.find('.owl-item').length > 3) {
          element.parent().find('.btn-next').click(function(){
            element.trigger('owl.next');
          });
          element.parent().find('.btn-prev').click(function(){
            element.trigger('owl.prev');
              console.log(element.attr('class'));
          });
        }
        else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }

      if(element.hasClass('style-2')) {

        element.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [960, 3], // betweem 900px and 601px
          itemsTablet: [768,3], //2 items between 600 and 0
          itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
        });

        if(element.find('.owl-item').length > 3) {
          element.parent().find('.btn-next').click(function(){
            element.trigger('owl.next');
          });
          element.parent().find('.btn-prev').click(function(){
            element.trigger('owl.prev');
          });
        }
        else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }

      if(element.hasClass('style-3')) {

        element.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [960, 3], // betweem 900px and 601px
          itemsTablet: [768,3], //2 items between 600 and 0
          itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
        });

        if(element.find('.owl-item').length > 3) {
          element.parent().find('.btn-next').click(function(){
            element.trigger('owl.next');
          });
          element.parent().find('.btn-prev').click(function(){
            element.trigger('owl.prev');
          });
        }

        else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }

      if(element.hasClass('style-6')) {

        element.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [960, 3], // betweem 900px and 601px
          itemsTablet: [768,3], //2 items between 600 and 0
          itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
        });

        if(element.find('.owl-item').length > 3) {
          element.parent().find('.btn-next').click(function(){
            element.trigger('owl.next');
          });
          element.parent().find('.btn-prev').click(function(){
            element.trigger('owl.prev');
          });
        }

        else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }

       if(element.hasClass('style-7')) {

        element.owlCarousel({
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [960, 3], // betweem 900px and 601px
          itemsTablet: [768,3], //2 items between 600 and 0
          itemsMobile : [320,1], // itemsMobile disabled - inherit from itemsTablet option
        });

        if(element.find('.owl-item').length > 3) {
          element.parent().find('.btn-next').click(function(){
            element.trigger('owl.next');
          });
          element.parent().find('.btn-prev').click(function(){
            element.trigger('owl.prev');
          });
        }

        else {
           element.parent().find('.btn-next, .btn-prev').addClass('hide');
        }
      }


    
     
    });

/*=======================================================
                        HEADER
=======================================================*/
$(window).scroll(function() {
if ($(this).scrollTop() > 1) {  
      $('header').addClass("sticky-header");

  }
  else{
      $('header').removeClass("sticky-header");
}
});



/*=======================================================
                    SERVICE GUARANTEE
=======================================================*/

$('#service-guarantee').waypoint(function() {
$('.service-guarantee-box').show().removeClass('wp2').addClass( 'animated wp2' );
},
{
offset: '80%',
});

$('#service-guarantee').waypoint(function() {
$('.service-guarantee-box').hide();
},
{
offset: '100%',
});


/*=======================================================
                        FANCYBOX
=======================================================*/
$(".members-fancybox").fancybox({
  wrapCSS: 'mbox',
  maxWidth: 1070,
  autoHeight: true,
  closeEffect: 'none',
  openEffect: 'fade',
  padding: 50,

  // beforeShow  : function() {
  //   $('#header').addClass('fancyOpen');
  // },
  // afterClose  : function(){
  //    $('#header').removeClass('fancyOpen');
  // }
});

$(".renders-fancybox").fancybox({
  autoHeight: true,
  closeEffect: 'none',
  openEffect: 'fade',
  // beforeShow  : function() {
  //   $('#header').addClass('fancyOpen');
  // },
  // afterClose  : function(){
  //    $('#header').removeClass('fancyOpen');
  // }
});


/*===========================================
Fancybox
===========================================*/
$('.fancybox').fancybox();


$('a[href*=#]').click(function (e) {
    e.preventDefault();

    var id = $(this).attr('href');
    var scrollTo = $(id).offset().top;

    $('html,body').animate({
        'scrollTop': scrollTo
    }, 800, 'swing');
});


/*=======================================================
                        WAYPOINT
=======================================================*/
$('.wp1').waypoint(function() {
    $('.wp1').addClass('animated fadeInLeft');
  }, {
    offset: '75%'
  });
  $('.wp2').waypoint(function() {
    $('.wp2').addClass('animated fadeInUp');
  }, {
    offset: '75%'
  });
  $('.wp3').waypoint(function() {
    $('.wp3').addClass('animated fadeInRight');
  }, {
    offset: '55%'
  });
  $('.wp4').waypoint(function() {
    $('.wp4').addClass('animated fadeInDown');
  }, {
    offset: '75%'
  });
  $('.wp5').waypoint(function() {
    $('.wp5').addClass('animated fadeInUp');
  }, {
    offset: '75%'
  });
  $('.wp6').waypoint(function() {
    $('.wp6').addClass('animated fadeInDown');
  }, {
    offset: '75%'
  });

});
