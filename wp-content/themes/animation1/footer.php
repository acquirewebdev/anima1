<footer>
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">

				<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 resetPadding">
					<div class="footer-section1">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-one-widget')) : endif; ?>
					</div>
					<div class="footer-section1">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-two-widget')) : endif; ?>
					</div>
					<div class="footer-section1">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-three-widget')) : endif; ?>
					</div>
					<div class="footer-section1">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-four-widget')) : endif; ?>
					</div>
					<div class="footer-section1">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-five-widget')) : endif; ?>
					</div>
				</div>
				
				<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 resetPadding">
					<div class="contact-form-bot">
						<!-- <h1 class="grey">TURN YOUR IDEAS INTO REALITY</h1>
						<h2 class="orange">CONTACT US TODAY</h2>
						<form>
							<div>
								<input type="text" placeholder="Fullname" />
								<input type="Email" placeholder="Email" />
							</div>
							<textarea placeholder="Message"></textarea>
							<input type="submit" value="Let's get started">
						</form> -->
						<div class="addbox">
							<h3>ANIMATION1</h3>
							<p>
								Level 15, 10 Queens Rd Melbourne 3004, Australia<br />
								AU: +61 3 8306 5700 | 1300 851 261<br /> 
								PH: +63 2 706 1437<br />
								info@animation1.com
							</p>
							<ul class="nav-socials-bot navbar-left">
								<li><a class="hvr-float fb" href=""></a></li>
								<li><a class="hvr-float vimeo" href=""></a></li>
								<li><a class="hvr-float twitter" href=""></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
					<div class="copyright">
						<?php echo copyright(); ?>
					</div>	
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>



</body>
</html>