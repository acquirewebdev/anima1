<?php get_header(); ?>

<section id="our-solutions">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="title-wrap-gray wp2 delay-05s">
					<h1 class=""><?php echo the_field('home-our-solutions'); ?></h1>
					<hr />
				</div>
				<div id="our-solutions-wrap-home">	
				<div class="top-solutions">
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('3d-animation'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], '3d-animation'); ?>
								 	<?php endif; ?>
								 </div>
							 	<div class="black">
									<h1><?php echo the_field('3d-animation-title'); ?></h1>
									<p><?php echo the_field('3d-animation-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('3d-animation-title'); ?></h1>
									<p><?php echo the_field('3d-animation-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/services/3d-animations/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>


						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('image-renders'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], 'image-renders'); ?>
								 	<?php endif; ?>
								 </div>
							 	<div class="black">
									<h1><?php echo the_field('image-renders-title'); ?></h1>
									<p><?php echo the_field('image-renders-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('image-renders-title'); ?></h1>
									<p><?php echo the_field('image-renders-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/services/3d-still-image-renders/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>
				
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('mobile-app'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], 'mobile-app'); ?>
								 	<?php endif; ?>
								</div>
							 	<div class="black">
									<h1><?php echo the_field('mobile-app-title'); ?></h1>
									<p><?php echo the_field('mobile-app-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('mobile-app-title'); ?></h1>
									<p><?php echo the_field('mobile-app-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/services/application-development/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="applications">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="title-wrap-gray">
					<h1>Applications</h1>
					<hr />
				</div>
				<p>Discover how our 3D visualisation solutions<br /> promote customer engagement, increase conversions and enhance brand awareness</p>
				
				<div id="app-box">
					<div id="app-list">
						<ul>
							<li><a href="<?php echo bloginfo('url'); ?>"><?php base64('ar.png','image'); ?></a></li>

							<li><a href="<?php echo bloginfo('url'); ?>"><?php base64('vr.png','image'); ?></a></li>

							<li><a href="<?php echo bloginfo('url'); ?>"><?php base64('pd.png','image'); ?></a></li>

							<li><a href="<?php echo bloginfo('url'); ?>"><?php base64('pm.png','image'); ?></a></li>

							<li><a href="<?php echo bloginfo('url'); ?>"><?php base64('avp.png','image'); ?></a></li>
						</ul>
				    </div>
					<hr />
					<div id="app-title-list">
						<ul>
							<li>
								<h1>Augmented Reality</h1>
								<a href="<?php echo bloginfo('url'); ?>">learn more</a>

							</li>
							<li>
								<h1>Virtual Reality</h1>
								<a href="<?php echo bloginfo('url'); ?>">learn more</a>
							
							</li>
							<li>
								<h1>Product demos</h1>
								<a href="<?php echo bloginfo('url'); ?>">learn more</a>
							
							</li>
							<li>
								<h1>Projection mapping</h1>
								<a href="<?php echo bloginfo('url'); ?>">learn more</a>
							
							</li>
							<li>
								<h1>AVPs & TVCs</h1>
								<a href="<?php echo bloginfo('url'); ?>">learn more</a>
								
							</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>



<section id="service-guarantee">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="title-wrap-gray">
					<h1 class="wp2 delay-05s"><?php echo the_field('home-service-gurantee'); ?></h1>
					<hr />
				</div>
				<div class="service-guarantee-wrap">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 resetPadding">
						<div class="service-guarantee-box hvr-fade wp2 delay-02s">
							<div class="hvr-float">
								<div class="sprite">
									<?php echo base64('superior.png', 'Superior Quality Visualisations', 'sprite-img'); ?>
								</div>
							</div>
									<h1><?php echo the_field('superior-quality-title'); ?></h1>
									<hr />
									<p><?php echo the_field('superior-quality-desc'); ?></p>
						</div>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 resetPadding">
						<div class="service-guarantee-box hvr-fade wp2 delay-03s">
							<div class="hvr-float">
								<div class="sprite">
									<?php echo base64('cutting-edge.png', 'cutting edge animation technology', 'sprite-img'); ?>
								</div>
							</div>
							<h1><?php echo the_field('cutting-edge-title'); ?></h1>
							<hr />
							<p><?php echo the_field('cutting-edge-desc'); ?></p>
						</div>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 resetPadding">
						<div class="service-guarantee-box hvr-fade wp2 delay-04s">
							<div class="hvr-float">
								<div class="sprite">
									<?php echo base64('industry.png', 'Industry-Leading Team', 'sprite-img'); ?>
								</div>
							</div>
							<h1><?php echo the_field('industry-leading-title'); ?></h1>
							<hr />
							<p><?php echo the_field('industry-leading-desc'); ?></p>
						</div>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 resetPadding">
						<div class="service-guarantee-box hvr-fade wp2 delay-05s">
							<div class="hvr-float">
								<div class="sprite">
									<?php echo base64('competitive.png', 'Competitive Packages', 'sprite-img'); ?>
								</div>
							</div>
							<h1><?php echo the_field('packages-title'); ?></h1>
							<hr />
							<p><?php echo the_field('package-desc'); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="clients">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="title-wrap-gray">
					<h1><?php echo the_field('home-clients'); ?></h1>
					<hr />
					<div id="testimonials" class="owlElement2 style-5">
					<?php 
						$cat = 3; //testimonials
						if (is_numeric($cat) ) :
							$catquery = new WP_Query(array('cat' => $cat));
								while($catquery->have_posts()) : $catquery->the_post();
						?>
							<div>
								<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
									<div class="testi-box-wrap">
										<div class="testibox-center">
											<p><?php the_content(); ?></p>
										</div>
									</div>
									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center resetPadding">
										<div class="testimg">
											<?php $image = get_field('testi-img'); 
												if(!empty($image)) : ?>
													<?php echo base64d($image['url'], 'Testimonials'); ?>
										 	<?php endif; ?>
									 		<h2><span class="orange"> <?php echo the_title(); ?>,</span> <?php echo the_field('testi-company'); ?></h2>
										 </div>
									</div>
								</div>
							</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
					</div>
				</div>
				<div style="float: left; width: 100%;">
					<div class="wrapper" style="position: relative;">
						<div class="customNavigation2">
							<a class="test-prev"><?php base64('btn-prev.jpg','',''); ?></a>
							<a class="test-next"><?php base64('btn-next.jpg','',''); ?></a>
						</div>
					</div>
				</div>
				<div id="clients-slider" class="owlElement2 style-4">
					<div class="item">
						<div class=""><?php echo base64('clients/boeing.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/glaxo.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/luminaut.png'); ?></div>
					</div>
					<div class="item ">
						<div class=""><?php echo base64('clients/marriot.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/mccann.png'); ?></div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/nestle.png'); ?></div>
					</div>
					<div class="item">
					<div class=""><?php echo base64('clients/nike.png'); ?></div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/pampers.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/pg.png'); ?></div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/som.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/sonylogo.png'); ?> </div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/shell.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/shang.png'); ?></div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/sheraton.png'); ?> </div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/westin.png'); ?></div>
					</div>
					<div class="item">
						<div class=" "><?php echo base64('clients/whotels.png'); ?></div>
					</div>
					<div class="item">
						<div class=""><?php echo base64('clients/zuelig.png'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="ideas-front">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-center resetPadding">
					<div class="ideas" class="text-center">
						<h3 class="inline-block">Bring your ideas to life</h3>
						<a class="getstarted inline-block" href="">Get started</a>
						<div id="form" style="display: none;">
							asd
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<?php /*
<section id="testimonials">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<!-- <h1><?php echo the_field('home-testimonials'); ?></h1> -->
				<div class="title-wrap-gray">
					<h1>Testimonials</h1>
					<hr />
				</div>
					<?php
					//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$wp_query = new WP_Query();
					$wp_query->query('cat=3&showposts=2&order=ASC');
					?>
					<?php if($wp_query->have_posts()) : ?>
						<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 resetPadding">
								<div class="testi-box-wrap">
									<div class="testibox-center">
										<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 resetPadding">
											<div class="testimg">
												<?php $image = get_field('testi-img'); 
													if(!empty($image)) : ?>
														<?php echo base64d($image['url'], 'Testimonials'); ?>
											 	<?php endif; ?>
											 </div>
										</div>
										<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 resetPadding">
											<div class="testibox">
												<p><?php the_content(); ?></p>
											</div>
										</div>
										<h1><span class="orange">- <?php echo the_title(); ?>,</span> <?php echo the_field('testi-company'); ?></h1>
									</div>
								</div>
							</div>
					<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</section>
*/ ?>
<?php /*
<section id="home-blog">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<!-- <h1><?php echo the_field('home-testimonials'); ?></h1> -->
				<div class="title-wrap-gray">
					<h1>BLOG</h1>
					<hr />
				</div>
					<?php
					//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$wp_query = new WP_Query();
					$wp_query->query('cat=4&showposts=2&order=DESC');
					?>
					
					<?php if($wp_query->have_posts()) : ?>
						<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
				
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 resetPadding">
							<div class="home-blog-wrap">
								<div class="home-blog-box">
									<div class="post-img-wrap">
										<?php if(function_exists('has_post_thumbnail') && has_post_thumbnail()) : ?>
											<?php echo the_post_thumbnail($post->ID); ?>
										<?php else: ?>
											<?php main_image(); ?>
										<?php endif; ?>
									</div>
									<?php $url = get_the_author_meta('author_profile_picture', $user_id);?>
									<div class="author">
									<!-- 	<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 resetPadding">
											<div class="author-pic">
												<img src="<?php echo $url; ?>" />
											</div>
										</div>
										<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 resetPadding">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div> -->
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
									<span><?php the_time(get_option('date_format')); ?>  by <font class="orange"><?php the_author(); ?></font></span>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						</div>
					
					<?php 
						endwhile; endif;  
					?>
					
			</div>
		</div>
	</div>
</section>
*/ ?>
<?php get_footer(); ?>