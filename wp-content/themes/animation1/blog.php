<?php 
/*
* Template Name: Blog Page
*/
get_header(); ?>
<section id="blog">
	<div class="resetPadding">
		<div class="wrapper">
			<div class="blog-wrap">
				<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
					<div class="blog-box">
						<?php
						$wp_query = new WP_Query();
						$wp_query->query('cat=4&showposts=5&order=DESC&paged='.$paged);
						?>
						<?php if($wp_query->have_posts()) : ?>
						<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>

						<div class="col-md-12 resetPadding">
							<div class="post-box">

								<?php /*<?php if(function_exists('has_post_thumbnail') && has_post_thumbnail()) : ?>
									<?php echo the_post_thumbnail($post->ID); ?>
								<?php else: ?>
									<?php main_image(); ?>
								<?php endif;  */ ?>
								<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 resetPadding">
									<div class="post-title">
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 col-xs-2 col-xs-2 pull-right resetPadding">
									<div class="post-like">
										<div class="like">
											<?php
												if(function_exists('like_counter_p')) { like_counter_p(''); }
											?>
										</div>
										<div class="dislike">
											<?php
												if(function_exists('dislike_counter_p')) { dislike_counter_p(''); }
											?>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 resetPadding">
									<span class="post-date">
										<?php the_time(get_option('date_format')); ?> - Posted by: <span class="orange"><?php the_author(); ?></span> to <?php foreach((get_the_category()) as $category) { $category->cat_name . ' '; } ?>
										<a href="<?php echo get_category_link(get_cat_id($category->cat_name)); ?>"><?php echo $category->cat_name ?></a>
									</span>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 resetPadding">
									<div class="post-content"><?php the_excerpt(); ?></div>
								</div>
								<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-right resetPadding">
									<div class="post-comment">
										<a class="orange" href="<?php echo the_permalink(); ?>">Read Full Entry</a> | <?php echo base64('comment.png','comment',''); ?> <a class="count" href="<?php the_permalink() ?>#disqus_thread">Comments</a>
									</div>
								</div>
							</div>
						</div>
						<?php 
						endwhile; endif;  
						blog_pagination();
						?>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
					<div class="sidebar-box">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>