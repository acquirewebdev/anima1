<?php 
/*
* Template Name: Services Page
*/
?>
<?php get_header(); ?>

<section class="single-banner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content">
					<h1 class="white"><?php the_title(); ?></h1>
						<p class="white">
							<span class="orange sourcebold">
							Bring your ideas to life with stunning visualisations from Animation1.</span> Choose from our wide range of visualisation, app development and augmented reality services.
						</p>
						<div class="banner-btn">
							<a href="<?php echo bloginfo('url'); ?>/contact-us/">GET STARTED</a>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="services">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="our-solutions-wrap">

					<div class="top-solutions">
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('3d-animation'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], '3d-animation'); ?>
								 	<?php endif; ?>
								 </div>
							 	<div class="black">
									<h1><?php echo the_field('3d-animation-title'); ?></h1>
									<p><?php echo the_field('3d-animation-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('3d-animation-title'); ?></h1>
									<p><?php echo the_field('3d-animation-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/3d-animations/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('image-renders'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], 'image-renders'); ?>
								 	<?php endif; ?>
								 </div>
							 	<div class="black">
									<h1><?php echo the_field('image-renders-title'); ?></h1>
									<p><?php echo the_field('image-renders-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('image-renders-title'); ?></h1>
									<p><?php echo the_field('image-renders-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/3d-still-image-renders/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>
					</div>

					<div class="bot-solutions">
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('mobile-app'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], 'mobile-app'); ?>
								 	<?php endif; ?>
								</div>
							 	<div class="black">
									<h1><?php echo the_field('mobile-app-title'); ?></h1>
									<p><?php echo the_field('mobile-app-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('mobile-app-title'); ?></h1>
									<p><?php echo the_field('mobile-app-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/mobile-applications/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>
						
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12  text-center resetPadding">
							<div class="img-solutions view third-effect wp1">
								<div class="img-solutions-wrap">
									<?php $image = get_field('augmented-reality'); 
									if(!empty($image)) : ?>
										<?php echo base64d($image['url'], 'augmented-reality'); ?>
								 	<?php endif; ?>
								 </div>
							 	<div class="black">
									<h1><?php echo the_field('augmented-reality-title'); ?></h1>
									<p><?php echo the_field('augmented-reality-desc'); ?></p>
								 </div>
								 <div class="mask">
									<h1><?php echo the_field('augmented-reality-title'); ?></h1>
									<p><?php echo the_field('augmented-reality-desc'); ?></p>
									<a href="<?php echo bloginfo('url'); ?>/3d-animations/augmented-reality/" target="_new" class="info">Learn More</a>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>