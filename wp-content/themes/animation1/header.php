<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>3D Animation Services | Architectural Renders | Animation1</title>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="index, follow">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?> charset=<?php bloginfo('charset'); ?>" /> 
<meta name='description' content="Animation1 is a premier visualisation studio providing 2D and 3D modeling, walkthroughs, augmented reality, interactive applications, medical and forensic animation."/>
<meta name="author" content="Animation1" /> 
<meta name="distribution" content="global" />    
<meta property='fb:app_id' content='426137180864625' />
<?php wp_head(); ?>
<!--styleheet-->
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" media="screen, projection" />
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<!--end-->
<!--[if lt IE 8]><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script><![endif]-->
<!--facebook tracking code-->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '816934645011535']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=816934645011535&amp;ev=PixelInitialized" /></noscript>
<!--end fb-->
</head>
<body <?php body_class(); //show body class ?> <?php if(is_page(34)) : echo 'onload="initialize()" onunload="GUnload()"'; endif; //google map for contact page ?>>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5552b1a946969ecc" async="async"></script>
<!--fb script-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=150195798399904";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end fb script-->
<div id="header">
<?php if(is_home() || is_front_page()): ?>
	<div class="overflow">
	<section id="logo-banner">
		<div class="container-fluid resetPadding">
			<div class="row resetPadding">
				<div class="wrapper">
					<div id="logo-banner-holder">
						<!-- <img src="<?php //bloginfo('template_directory'); ?>/images/logo-banner.png" />
						<img src="data:image/png;base64,<?php //echo base64('logo-banner.png'); ?>"  /> -->
						<?php base64('logo-banner.png','logo','wp2'); ?>
						<h1 class="wp2"><?php echo the_field('home-title-banner'); ?></h1>
						<hr class="wp2"/>
						<p class="wp2"><?php echo the_field('home-title-p'); ?></p>
						<!-- <a class="real" href="#"><?php echo the_field('home-title-btn'); ?></a> -->
						<a class="getstarted wp2" href="<?php bloginfo('url'); ?>/contact-us/"><?php echo the_field('get-started-btn'); ?></a>
						<div id="btnArrow" class="wp2">
							<a href="#our-solutions">
								<?php $image = get_field('home-btn-arrow'); 
								if(!empty($image)) : ?><!-- 
								<img src="<?php //echo $image['url']; ?>" /> -->
								<?php echo base64d($image['url'], 'arrow',''); ?>
							 	<?php endif; ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- <div id="ri-grid" class="ri-grid ri-grid-size-3">
		<ul>
			<li><a href="#"><?php base64('image01.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image02.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image03.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image04.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image05.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
			<li><a href="#"><?php base64('image06.jpg','image'); ?></a></li>
		</ul>
	</div> -->
	<div id="demoreel">
		<video autoplay loop width="100%" height="100%" preload="auto">
			<source src="<?php bloginfo('template_directory'); ?>/video/animation1.mp4" type="video/mp4" />  
			<source src="<?php bloginfo('template_directory'); ?>/video/animation1.ogv" type="video/ogg" />    
			<source src="<?php bloginfo('template_directory'); ?>/video/animation1.webm" type="video/webm" />   
		</video>
	</div>
		<div id="overlay"></div>
	</div>
<?php endif; ?>
<?php if(is_home() || is_front_page()): ?>
<header>
<?php else: ?>
<header class="single-header">
<?php endif; ?>
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<nav class="navbar navbar-default" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<div class="logowrap">
							<div class="logo">
								<a href="<?php echo home_url(); ?>">
								<?php base64('logo.png','Animation1 Logo'); ?>
								</a>
							</div>
						</div>
						<div class="menuwrap">
						<?php
							if( has_nav_menu( 'header-menu' ) ) { /* if menu location 'primary-menu' exists then use custom menu */
								wp_nav_menu( array( 
									'theme_location' => 'header-menu',
									'items_wrap'     => '<ul class="nav navbar-nav navbar-left">%3$s</ul>'
							  )); 
							}
						?>
						</div>
						<div class="navsocialswrap">
							<ul class="nav-socials">
			 					<li class="inline-block"><span class="orange">CALL US:</span>  <span class="grey2">+61 3 8306 5700</span></li>
			 					<li class="right"><a class="hvr-float fb" href=""></a></li>
			 					<li class="right"><a class="hvr-float vimeo" href=""></a></li>
			 					<li class="right"><a class="hvr-float twitter" href=""></a></li>
							</ul>
						</div>
					<!-- </ul> -->
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>
</header>

</div>	