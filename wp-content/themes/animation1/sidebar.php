<div class="col-md-12 resetPadding">
	<div class="sidebar-box">
		<div class="checkout-box">
			<h1>Check Out Animation1</h1>
			<hr />
			<p>We offer a broad spectrum of animation and visualisation services to turn brands and projects into memorable cinematic experiences. Talk to the leaders in animation today!</p>
			<div class="checkout-button">
				<a href="<?php echo bloginfo('url'); ?>/contact-us/">Let's Talk</a>
			</div>
		</div>
	</div>
	<div class="sidebar-nobox">
		<div class="categories-box">
			<h1>Categories</h1>
			<hr />
			<ul>
				<li><a href="">News</a></li>
				<li><a href="">Animation</a></li>
				<li><a href="">Augmented Reality</a></li>
				<li><a href="">Visual Effects</a></li>
			</ul>
		</div>
	</div>
	<div class="sidebar-nobox">
		<div class="categories-box">
			<h1>Join the Animation1 Social Community</h1>
			<hr />
			<!-- Go to www.addthis.com/dashboard to customize your tools -->
			<div style="display: inline-block;" class="addthis_horizontal_follow_toolbox"></div>
		</div>
	</div>	
	<div class="sidebar-nobox">
		<div class="categories-box">
			<h1>Social Connection</h1>
			<hr />
			<div class="fb-page" data-href="https://www.facebook.com/Animation1Official" data-width="380" data-height="304" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Animation1Official"><a href="https://www.facebook.com/Animation1Official">Animation1</a></blockquote></div></div>
		</div>
	</div>
</div>