<?php 
/*
* Template Name: About Page
*/
?>
<?php get_header(); ?>

<section class="single-banner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content">
					<h1 class="white"><?php the_title(); ?></h1>
						<p class="white"><span class="orange sourcebold">Creative, experienced, full-time artists and professionals</span> deliver a range of services including architectural renders, Augmented Reality, application development and other digital services to turn brands and projects into memorable, virtual experiences.</p>
						<div class="box-banner">
							<a href="<?php echo bloginfo('url'); ?>/contact-us/">GET STARTED</a>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="about">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1 class="orange">ABOUT COMPANY</h1>
				<hr />
				<p>
					Animation1 aims to incite a revolution – to discern new markets for animation, to re-discover and extend the boundaries of customer satisfaction and to lead the international animation industry. Animation1 aims to be the best in the country, at par with the world, internationally accessible and committed to providing our clients with a sensibly euphoric Customer Experience.
				</p>
			</div>
		</div>
	</div>
</section>
<section id="about-one">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<h1> CORE PURPOSE </h1>
			<hr style="max-width: 145px !important; " />
			<p>
				To inspire innovation through Animation. Animation1 provides the visual creativity for our clients to turn their ideas into reality. 
			</p>
			<br /><br />
			<h1> VISION </h1>
			<hr style="max-width: 70px !important; " />
			<p>
				The tools we build with, the processes we implement, the staff and employees we work with, are the resources we use to redefine the visualization paradigm. All of us together have created a firm that reinvents the way creative energy is internalized, materialized, and shared with our clients in a partnering business model. 
			</p>
			<br />
			<p>
				We will always be at the forefront of our industry, always use our accomplishments to direct the attention of the world to our country, continually use our skills to bring pride to our motherland and hone our talents to strengthen our industry. Our success is propelled by our desire to make a difference in society, to inspire generosity in our community building program and to challenge our team to achieve excellence.
			</p>

			</div>
		</div>
	</div>
</section>
<!-- <section id="about-two">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1 class="orange">WHO WE ARE</h1>
				<hr />
				<p>
				Animation1 aims to incite a revolution – to discern new markets for animation, to re-discover and extend the boundaries of customer satisfaction and to lead the international animation industry. Animation1 aims to be the best.		</p>
			</div>
		</div>
	</div>
</section> -->
<section id="about-two">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1 class="orange">WHO WE ARE</h1>
				<hr />
				<p>
					Animation1 aims to incite a revolution – to discern new markets for animation, to re-discover and extend the boundaries of customer satisfaction and to lead the international animation industry. Animation1 aims to be the best.
				</p>

				<div id="members-wrap">
				
				<div id="about-members-top">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
								<!--scott stavretis-->
								<div class="members">
									<a href="#scott-desc" class="members-fancybox">
										<div class="members-box">
										<?php echo base64('members/scott-stavretis.jpg'); ?>
										<h2>SCOTT STAVRETIS</h2>
										<span>Chief Executive Officer</span>
										</div>
									</a>
								</div>
								<div id="scott-desc" class="hide-box">
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
										<?php echo base64('members/scott-stavretis.jpg'); ?>
									</div>
									<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
										<h1>SCOTT STAVRETIS</h1>
										<span>Chief Executive Officer</span>
										<p>Scott Stavretis is the CEO and a founding director of Acquire with over 15 years of executive management experience. Throughout his career, he has lead multiple start-up companies as well as large organisations through aggressive growth phases and is a highly regarded senior executive for both private and public listed companies in the telecommunications industry. 
										<br /><br />
										Scott was the COO and Executive Manager of Dodo, Australia’s largest private telecommunications company, since its inception and was pivotal in Dodo’s rapid growth trajectory. Scott also was responsible for the transformation of Dodo from a consumer telecommunications provider to a national consumer brand with the formation of Dodo Power & Gas. With the launch of Dodo Insurance and Dodo Security, Dodo became the only provider in Australia with such a diverse offering of utilities and services to consumers. 
										<br /><br />
										Scott led the reverse takeover of Eftel Ltd, an ASX listed telecommunications company, in 2011 and served as its CEO and Managing Director. His contributions to the industry granted him one of the most prestigious awards in telecommunications – ACCOMS Young Achiever award in 2012. During his time at Eftel he tripled the revenue in less than three years prior to the sale of Eftel to M2, at which time he also led the Dodo M&A team where both Dodo and Eftel were subsequently sold to M2 in 2013 for $250M. 
										<br /><br />
										Scott was one of the early pioneers of the Internet industry in Australian founding Connect Direct Internet in 1996, which he later sold. Scott has also held leadership roles in diverse industries for companies like Bet247, which became the most rapid growing sports betting company in the country prior to being sold to Tom Waterhouse. Scott was also started Pendo Industries and Infinite Rewards and is also currently the CEO of global 3d animation and augmented reality company Animation1.</p>
									</div>
								</div>
								<!--ed travis-->
								<div class="members" >
									<a href="#ed-desc" class="members-fancybox">
									<div class="members-box">
										<?php echo base64('members/ed-travis.jpg'); ?>
										<h2>ED TRAVIS</h2>
										<span>General Manager Animation1</span>
									</div>
									</a>
								</div>
								<div id="ed-desc" class="members-fancybox hide-box">
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
										<?php echo base64('members/ed-travis.jpg'); ?>
									</div>
									<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
										<h1>ED TRAVIS</h1>
										<span>General Manager Animation1</span>
										<p>Prior to joining Animation1, Ed held a variety of roles, including Associate Partner for the Strategy Consultancy of Accenture, as well as holding a Director’s role for a strategic investment of Temasek, the holding company for the Government of Singapore. As the General Manager of Animation1, he administers all operations including staff, equipment, production, and processes for the animation studio with a business model patterned after a Knowledge Processing Outsourcing organization. He undertook global and local marketing and sales initiatives to diversify the core product mix, which was originally centred upon Architectural Animation, to include key business units in Product Animation, Infrastructure development, Medical Animation, Gaming and Entertainment, and Augmented Reality applications.</p>
									</div>
								</div>
								<!--len moreto-->
								<div class="members">
									<a href="#len-desc" class="members-fancybox">
									<div class="members-box">
										<?php echo base64('members/len-moreto.jpg'); ?>
										<h2>LEN MORETO</h2>
										<span>Chief Financial Officer</span>
									</div>
									</a>
								</div>
								<div id="len-desc" class="hide-box">
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
										<?php echo base64('members/len-moreto.jpg'); ?>
									</div>
									<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
										<h1>LEN MORETO</h1>
										<span>Chief Financial Officer</span>
										<p>Len joins the Animation1 team from her former role at SITEL as Director of Finance. She was one of the pillars of SITEL Philippines having been part of the start-up team when it first launched. She was responsible for setting up financial processes from financial reporting and accounting, financial planning, budget and treasury, payroll, to asset management and purchasing. Len joined Acquire with the same objective and has re-engineered the finance function and delivery of the Company. Now, Animation1 has seized the opportunity to utilize this highly experienced and well versed CFO to securely propel its company’s financials.</p>
									</div>
								</div>
								<!--kathryn marshall-->
								<div class="members">
									<a href="#kathryn-desc" class="members-fancybox">
									<div class="members-box">
										<?php echo base64('members/kathryn-marshall.jpg'); ?>
										<h2>KATHRYN MARSHALL</h2>
										<span>General Manager Sales</span>
									</div>
									</a>
								</div>
								<div id="kathryn-desc" class="hide-box">
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
										<?php echo base64('members/kathryn-marshall.jpg'); ?>
									</div>
									<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
										<h1>KATHRYN MARSHALL</h1>
										<span>General Manager Sales</span>
										<p>As the General Manager of Sales for Acquire Asia Pacific, and a key contributor to the senior leadership team, Kathryn Marshall continually builds upon new and existing sales channels through an extensive array of global networks. The acquisition of Animation1 provides a diverse range of new solutions available to a multitude of international clients through these channels. Kathryn Marshall now serves as the General Manager of sales for Animation1, and aims to use her extensive network to propel Animation1’s international growth.</p>
									</div>
								</div>
							</div>
						</div>
					
					<div class="col-md-12 resetPadding">
						<div id="about-members-bot">
							<div id="about-members-bot-wrap">
								<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resetPadding">
									<!--dana andel-->
									<div class="members">
										<a href="#dana-desc" class="members-fancybox">
										<div class="members-box">
											<?php echo base64('members/dana-andel.jpg'); ?>
											<h2>DANA ANDEL</h2>
											<span>General Manager Philippines</span>
										</div>
										</a>
									</div>
									<div id="dana-desc" class="hide-box">
										<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
											<?php echo base64('members/dana-andel.jpg'); ?>
										</div>
										<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
											<h1>DANA ANDEL</h1>
											<span>General Manager Philippines</span>
											<p>Dana Andel is arguably the leading authority in contact centre and customer service leadership in the Philippines. He is responsible for managing the shared services layer of Acquire and Animation1. Dana brings with him over 18 years of management experience, ensuring the right people are hired and the right processes are followed in managing the company’s’ Philippines branch.</p>
										</div>
									</div>
									<!--taj chail-->
									<div class="members">
										<a href="#taj-desc" class="members-fancybox">
										<div class="members-box">
											<?php echo base64('members/taj-chail.jpg'); ?>
											<h2>TAJ CHAIL</h2>
											<span>General Manager Marketing</span>
										</div>
										</a>
									</div>
									<div id="taj-desc" class="hide-box">
										<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
											<?php echo base64('members/taj-chail.jpg'); ?>
										</div>
										<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
											<h1>TAJ CHAIL</h1>
											<span>General Manager Marketing</span>
											<p>Taj has over 10 years of marketing management experience with extensive expertise in online and digital marketing. Prior to joining Acquire in 2009, she was the Group Marketing Manager for Dodo, Eftel, Club Telco and Bet247. Prior to that, Taj worked at Sensis within product management. Taj now takes on the role as the General Manager of Marketing for Animation1.</p>
										</div>
									</div>
									<!--andy king-->
									<div class="members">
										<a href="#andy-desc" class="members-fancybox">
										<div class="members-box">
											<?php echo base64('members/andy-king.jpg'); ?>
											<h2>ANDY KING</h2>
											<span>Chief Technology Officer</span>
										</div>
										</a>
									</div>
									<div id="andy-desc" class="hide-box">
										<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 resetPadding">
											<?php echo base64('members/andy-king.jpg'); ?>
										</div>
										<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12 resetPadding">
											<h1>ANDY KING</h1>
											<span>Chief Technology Officer</span>
											<p>Andy has joined Acquire recently as the CTO bringing a wealth of experience and expertise from decades of senior leadership experience in the IT and telco industry. A seasoned industry veteran, Andy founded one of the largest ISP in Australia, Edge Internet Services Australia in 1995, before joining the of ranks of CISCO, Telstra, CSC, Telarus, NAB, Victorian & Federal Government departments in various technology leadership and strategic consulting roles. Andy well respected amongst his peers being at the forefront of technology innovation and world first deployments of technologies such as GPRS, Unified Messaging and IP Contact Centre systems. His interests in innovation extends past technology into scientific research in energy, transportation as an active board member of several companies. Andy is responsible for the evolution of Acquire’s global IT& T infrastructure and software.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>