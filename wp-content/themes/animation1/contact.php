<?php
session_start();
/*
* Template Name: Contact Page Template
*/
get_header(); 
?>
<section class="single-banner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content">
					<h1 class="white"><?php the_title(); ?></h1>
					<p class="white"><span class="orange sourcebold">Creative, experienced, full-time artists and professionals</span> deliver a range of services including architectural renders, Augmented Reality, application development and other digital services to turn brands and projects into memorable, virtual experiences.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contact">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div id="contact-wrapper">
					<h1 class="orange">Let's Chat</h1>
						<?php echo do_shortcode('[sf_form_contact_website]'); ?>
						
							<p>
								<span class="orange regular">Head Office:</span> Level 15, 10 Queens Rd Melbourne 3004, Australia
							</p>
							<p>
								<?php $country = geoip_detect_get_info_from_current_ip();
									if ($country && $country->country_code == 'US') { echo "<span class='orange regular'>United States :</span> +61 3 8306 5700"; } 
									else if($country && $country->country_code == 'PH') { echo "<span class='orange regular'>Philippines:</span> +63 2 706 1437"; } 
									else if ($country && $country->country_code == 'GB') { echo "<span class='orange regular'>United Kingdom:</span> +61 3 8306 5700"; }
									else if ($country && $country->country_code == 'AU') { echo "<span class='orange regular'>Australia:</span> +61 3 8306 5700 | 1300 653 264 <br /> Philippines: +63 2 706 1437"; }
									else { echo "<span class='orange regular'>Australia:</span> +61 3 8306 5700 | 1300 653 264"; } 
								?> 
							</p>
							<p>	
								<span class="orange regular">Email:</span> <a href="mailto:info@animation1.com">info@animation1.com</a>
							</p>

					<ul class="nav-socials-bot text-center">
	 					<li><a class="hvr-float fb" href=""></a></li>
	 					<li><a class="hvr-float vimeo" href=""></a></li>
	 					<li><a class="hvr-float twitter" href=""></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="map">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<!--google map-->
			<script src="http://maps.google.com/maps/api/js?v=3.5&sensor=false"></script>
			<script type="text/javascript">
			function initialize() {
				var leeds = new google.maps.LatLng(-37.8381714, 144.9744961);
				var firstLatlng = new google.maps.LatLng(-37.8381714, 144.9744961);              
				var firstOptions = {
				    zoom: 16,
				    center: firstLatlng,
				    mapTypeId: google.maps.MapTypeId.ROADMAP 
				};

				var map = new google.maps.Map(document.getElementById("map_leeds"), firstOptions);
				firstmarker = new google.maps.Marker({
				    map:map,
				    draggable:false,
				    animation: google.maps.Animation.DROP, 
				    title: 'Animation1',
				    position: leeds
				});

				var contentString1 = '<p>Head Office: Level 15, 10 Queens Rd Melbourne 3004, Australia</p>';
				var infowindow1 = new google.maps.InfoWindow({
				    content: contentString1
				});
				google.maps.event.addListener(firstmarker, 'click', function() {
				    infowindow1.open(map,firstmarker);
				});
			}
			</script>
		<div class="map">
		    <div id="map_leeds" style="width: 100%; height: 370px;"></div>  
		</div>
		<!--end google map-->
	</div>
</div>
</section>
<?php get_footer(); ?>