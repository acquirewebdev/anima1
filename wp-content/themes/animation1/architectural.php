<?php 
/*
* Template Name: Architectural Page
*/
?>
<?php get_header(); ?>

<section id="architectural-banner-inner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content-inner">
					<h1 class="white">ARCHITECTURE</h1>
					<p>Experienced artists and professionals work together to create the most advanced visualisation solutions meeting 
the standards of a wide variety of industries. </p>
						<!-- <div class="menu-top-menu-wrap">
							<?php 
								$args = array(
								    'menu'    => 'top-menu',
								    'submenu' => 'Industry',
								);
								wp_nav_menu( $args );
							?>
						</div> -->
					<!-- <div class="bannerbtn">
						<a class="text-center " href="<?php bloginfo('url'); ?>/contact-us/">Get started</a>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section>
<?php /*
<section id="architectural-inner">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1>Highways, airports, bridges, ports, and other infrastructure take a lot of planning and a lot of resources to build. Physical and functional characteristics have to be taken into consideration in order to drive analysis, schedules, take-off and logistics. Computer-generated, three-dimensional (3D) visualisations transform these considerations into lifelike representations, which support concept-to-construction decision-making. </h1>
			<!-- 	<?php $image = get_field('industry-inner-img'); 
					if(!empty($image)) : ?>
					<?php echo base64d($image['url'], 'industry image'); ?>
				<?php endif; ?> -->
				<?php echo base64('industry-img.png'); ?>
			</div>
		</div>
	</div>
</section>
*/ ?>

<section id="industry-inner-two">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
				
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 resetPadding">
						<div class="industry-left-inner">
							<h1 class="orange">3D still image renders</h1>
							<p>Highlight the three primary spatial dimensions: width, height and depth, with our 3D still image renders. Apart from spatial relationships, our visualisations also showcase light analysis and geographic information, which help developers better communicate ideas, accelerate approval and input adjustments.</p>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 resetPadding">
						<div class="slider-wrapper">
							<div class="architectural-inner-sub-works style-6 owlElement">
								<?php 
									$cat = 33;
									if (is_numeric($cat) ) :
										$catquery = new WP_Query(array('cat' => $cat));
											while($catquery->have_posts()) : $catquery->the_post();
								?>
								<div class="list-works">
									<div class="itemz view third-effect-black">
										<?php 
										
										$image = get_field('testi-img'); 
											if(!empty($image)) : ?>
											<?php echo base64d($image['url'], ''); ?>


										<div class="works-mask">
											<a class="renders-fancybox" href="<?php echo $image['url']; ?>"><?php echo base64('view-img2.png','','view-item');  ?></a>
										</div>
												<?php endif; ?>
										<div class="animation-works-title">
											<h2><?php the_title(); ?></h2>
										</div>
									</div>
								</div>
								<?php
										wp_reset_query();
										endwhile;
									endif;	
								?>
							</div>
							<div class="customNavigation">
								<a class="btn-prev"><?php base64('btn-prev-grey.jpg','',''); ?></a>
								<a class="btn-next"><?php base64('btn-next-grey.jpg','',''); ?></a>
							</div>
							<div class="project-btn">
								<a class="getstarted text-right" href="#">View all projects</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="industry-inner-one">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 resetPadding">
						<div class="industry-left-inner">
							<h1 class="orange">3D walkthroughs & flythroughs</h1>
							<p>Make it easier for most people to understand what 2D elevations and 3D infrastructure plans cannot reveal. Portray colour hues, light intensity, light reflection and the play of shadows, which reflect interior or exterior lighting. Also depict perspective, structure, materials, textures, finishes, as well as external environments, thereby helping professionals reduce uncertainty, improve safety, work out problems and simulate and analyse potential impacts prior to the actual physical construction.</p>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 resetPadding">
						<div class="slider-wrapper">
							<div class="architectural-inner-sub-works style-6 owlElement">
								<?php 
									$cat = 34;
									if (is_numeric($cat) ) :
										$catquery = new WP_Query(array('cat' => $cat));
											while($catquery->have_posts()) : $catquery->the_post();
								?>
								<div class="list-works">
									<div class="itemz view third-effect-black">
										<?php 
										//echo the_title();
											if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
												echo base64d($video_thumbnail,'animation1 video',''); 
											} 
										?>
										<div class="works-mask">
											<a class="fancybox fancybox.iframe" href="<?php echo the_field('video-link'); ?>">	<?php echo base64('play.png','','view-item');  ?></a>
										</div>
										<div class="animation-works-title">
											<h2><?php the_title(); ?></h2>
										</div>
									</div>
								</div>
								<?php
									wp_reset_query();
									endwhile;
								endif;	
							?>
							</div>
							<div class="customNavigation">
								<a class="btn-prev"><?php base64('btn-prev-grey.jpg','',''); ?></a>
								<a class="btn-next"><?php base64('btn-next-grey.jpg','',''); ?></a>
							</div>
							<div class="project-btn">
								<a class="getstarted text-right" href="#">View all projects</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- 
<section id="industry-works">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<h1 class="orange text-center">SOME OF OUR WORKS</h1>
			<div id="industry-works-wrap">	
				<div class="wrapper">
						<div class="services-inner-sub-works">
						<?php 
							if (is_page(160) ) :
								$cat = 8; //Architectural
							elseif (is_page(162) ) :
								$cat = 9; //Infrastructure
							elseif (is_page(164) ) :
								$cat = 10; //Medical
							elseif (is_page(166) ) :
								$cat = 11; //Forensics
							elseif (is_page(168) ) :
								$cat = 12; //Entertainment & Cinema
							endif;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="itemz view third-effect-black">
							<?php 
							//echo the_title();
							
								if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
									echo base64d($video_thumbnail,'animation1 video',''); 
								} 
							?>
							<div class="mask">
								<a href="<?php echo bloginfo('url'); ?>" target="_new" class=""><?php echo base64('view-img.png','','view-item');  ?></a>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
					</div>
					<div class="customNavigation">
						<a class="btn-prev"><?php base64('btn-prev.jpg','',''); ?></a>
						<a class="btn-next"><?php base64('btn-next.jpg','',''); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section id="architectural-inner-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-center resetPadding">
					<div class="ideas" class="text-center">
						<h3 class="inline-block">Give your project a solid start</h3>
						<a class="getstarted inline-block fancybox" href="#form">Talk to us</a>
						<div id="form" style="display: none;">
							asd
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>