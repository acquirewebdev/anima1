<?php 
/*
* Template Name: Industry Inner Page
*/
?>
<?php get_header(); ?>

<section class="single-banner-inner">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div class="banner-content-inner">
					<h1 class="white"><?php the_title(); ?></h1>
						<div class="menu-top-menu-wrap">
							<?php 
								$args = array(
								    'menu'    => 'top-menu',
								    'submenu' => 'Industry',
								);
								wp_nav_menu( $args );
							?>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="industry-inner">
<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<h1><?php the_field('industry-inner-h1'); ?></h1>
				<p><?php the_field('industry-inner-p'); ?></p>
				<?php $image = get_field('industry-inner-img'); 
					if(!empty($image)) : ?>
					<?php echo base64d($image['url'], 'industry image'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section id="industry-clients">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
				<h1 class="orange text-center">SOME OF OUR CLIENTS</h1>
			<div id="industry-clients-slider-wrap">	
				<div class="wrapper">
					<div id="industry-clients-slider">
						<div class="items"><?php echo base64('clients/boeing.png'); ?></div>
						<div class="items"><?php echo base64('clients/dentsu.png'); ?></div>
				
						<div class="items"><?php echo base64('clients/glaxo.png'); ?></div>
						<div class="items"><?php echo base64('clients/luminaut.png'); ?></div>
				
						<div class="items"><?php echo base64('clients/marriot.png'); ?></div>
						<div class="items"><?php echo base64('clients/mccann.png'); ?></div>
					
						<div class="items"><?php echo base64('clients/nestle.png'); ?></div>
						<div class="items"><?php echo base64('clients/nike.png'); ?></div>
					
						<div class="items"><?php echo base64('clients/pampers.png'); ?></div>
						<div class="items"><?php echo base64('clients/pg.png'); ?></div>
				
						<div class="items"><?php echo base64('clients/som.png'); ?></div>
						<div class="items"><?php echo base64('clients/sonylogo.png'); ?> </div>
					
						<div class="items"><?php echo base64('clients/shell.png'); ?></div>
						<div class="items"><?php echo base64('clients/shang.png'); ?></div>
				
						<div class="items"><?php echo base64('clients/sheraton.png'); ?> </div>
						<div class="items"><?php echo base64('clients/westin.png'); ?></div>
				
						<div class="items"><?php echo base64('clients/whotels.png'); ?></div>
						<div class="items"><?php echo base64('clients/zuelig.png'); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="industry-inner-one">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-left-inner top49">
							<h1 class="orange"><?php the_field('industry-inner-one-h1'); ?></h1>
							<p><?php the_field('industry-inner-one-p'); ?></p>
							<a class="text-right pull-right industry-inner-btn" href=""><?php the_field('industry-inner-btn'); ?></a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-right-inner">
								<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="industry-inner-two">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="industry-left-inner ">
							<?php echo base64('video-thumb.png'); ?>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 resetPadding">
						<div class="top49 industry-right-inner">
							<h1 class="orange"><?php the_field('industry-inner-two-h1'); ?></h1>
							<p><?php the_field('industry-inner-two-p'); ?></p>
							<a class="text-right pull-right industry-inner-btn" href=""><?php the_field('industry-inner-btn'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="industry-works">
	<div class="containter-fluid resetPadding">
		<div class="row resetPadding">
			<h1 class="orange text-center">SOME OF OUR WORKS</h1>
			<div id="industry-works-wrap">	
				<div class="wrapper">
						<div class="services-inner-sub-works">
						<?php 
							if (is_page(160) ) :
								$cat = 8; //Architectural
							elseif (is_page(162) ) :
								$cat = 9; //Infrastructure
							elseif (is_page(164) ) :
								$cat = 10; //Medical
							elseif (is_page(166) ) :
								$cat = 11; //Forensics
							elseif (is_page(168) ) :
								$cat = 12; //Entertainment & Cinema
							endif;
							if (is_numeric($cat) ) :
								$catquery = new WP_Query(array('cat' => $cat));
									while($catquery->have_posts()) : $catquery->the_post();
						?>
						<div class="itemz view third-effect-black">
							<?php 
							//echo the_title();
							
								if( ( $video_thumbnail = get_video_thumbnail() ) != null ) { 
									echo base64d($video_thumbnail,'animation1 video',''); 
								} 
							?>
							<div class="mask">
								<a href="<?php echo bloginfo('url'); ?>" target="_new" class=""><?php echo base64('view-img.png','','view-item');  ?></a>
							</div>
						</div>
						<?php
								wp_reset_query();
								endwhile;
							endif;	
						?>
					</div>
					<div class="customNavigation">
						<a class="btn-prev"><?php base64('btn-prev.jpg','',''); ?></a>
						<a class="btn-next"><?php base64('btn-next.jpg','',''); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="industry-inner-three">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">	
				<div class="industry-inner-wrap">
					<div id="industry-inner-box">
						<h1><?php the_field('industry-inner-three-h1'); ?></h1>
						<p><?php the_field('industry-inner-three-p'); ?></p>
						<div class="industry-inner-btn-large-wrap">
							<a class="text-center services-inner-btn-large" href=""><?php the_field('industry-inner-btn-large'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>