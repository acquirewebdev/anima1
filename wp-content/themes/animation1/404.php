<?php get_header(); ?>
<section id="error">
	<div class="container-fluid resetPadding">
		<div class="row resetPadding">
			<div class="wrapper">
				<div id="error-content">
					<h1>Nope, there's nothing here.</h1>
					<p>This might not be the page that you're looking for.</p>
					<a href="<?php echo home_url(); ?>">GO BACK HOME</a>
					<hr />
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>